Frozen Lair - A OGRE3D Demo
===

Welcome to our little project. This short game stems from a university project for a computer graphics course. We were the tasked to choose a theme and then build:

* A 3D scene render using Blender.
* A 3D application using any framework of choice, no advanced engines allowed (no Unity, Unreal etc.).

This here is the result of our work. We chose to create a small adventure game, where the player (a treasure hunter) enters the domain of an ice dragon in order to steal its treasure, while the render is a more sophisticated version of the same setting. You can download the compiled and running version of the game inside the Downloads section of this repository. The game runs under Windows; be sure to download the latest VisualC++ 2017 redistributable package if you haven't. A link can be found inside the game's README file. Other files inside this repository include the game's source, the render's models and results.

Gameplay Video
---

[![FrozenLair Gameplay](https://img.youtube.com/vi/AABzTGtd5-4/0.jpg)](https://www.youtube.com/watch?v=AABzTGtd5-4)

## Compilation notes

The game runs on a number of frameworks which are required for the compilation to succeed. Most of these packages can be downloaded and built using [VCPKG](https://blogs.msdn.microsoft.com/vcblog/2016/09/19/vcpkg-a-tool-to-acquire-and-build-c-open-source-libraries-on-windows/).

* OGRE [v1.9]  
    https://bitbucket.org/sinbad/ogre/overview
    https://bitbucket.org/cabalistic/ogredeps

    Be sure to compile version 1.9. The second link includes dependencies to build OGRE.
    
* CEGUI [v0-8]  
    https://bitbucket.org/cegui/cegui  
    https://bitbucket.org/cegui/cegui-dependencies  
    
    Same as OGRE, build first the dependencies from the second link, then proceed compiling CEGUI 0.8 following the provided instructions. Choose ExpatParser as default XML parser, build the FreeImage Image Codec and the Ogre Renderer.

* BulletPhysics [2.86]  
    https://github.com/bulletphysics/bullet3
    
* SFML [2.4.2]  
    https://github.com/SFML/SFML

## Additional dependencies

https://bitbucket.org/double_dragon/frozen-lair-dependencies

In this repository we put some extra tools which we modified to get a working environment between Blender and OGRE. Please refer to its Readme for further instructions.

### Complete list of .lib requirements

sfml-system.lib  
sfml-audio.lib  
BulletDynamics.lib  
BulletCollision.lib  
LinearMath.lib  
Bullet3Dynamics.lib  
Bullet3Collision.lib  
Bullet3Common.lib  
CEGUIBase-0.lib  
CEGUIFreeImageImageCodec.lib  
CEGUIExpatParser.lib  
CEGUIOgreRenderer-0.lib  
dotSceneInterface.lib  
tinyxml.lib  
OgreMain.lib  
OgreOverlay.lib  
OIS.lib  

Frozen Lair - The Cave
===

All the modeling work was done with Blender 2.78a.

In addition to the very simple and low poly scene developed for the game demo, we also developed a more complex version of the same 3D environment in which we experimented with:

* Physics behaviour

* Cloth simulation

* Fire simulation

* Particle systems

* Rigging

* Node Texturing

* Lighting techniques (Volume scattering, transparency, translucency, refraction, etc.)


All the objects in the scene were created by us from scratch.
Here are some renderings of the scene:


![Alt text](https://bytebucket.org/double_dragon/frozen-lair/raw/e35a299338bc1d5e0ddc33c918ba14568e6b14d4/Renders/topdef.png "Top View")

![Alt text](https://bytebucket.org/double_dragon/frozen-lair/raw/e35a299338bc1d5e0ddc33c918ba14568e6b14d4/Renders/botdefdef.png "Bottom View")


# About the authors

If you like our work, feel free to browser our other projects!

* Alessio Giuseppe Calì- Game Programming  
    GitHub: https://github.com/alessiocali/  
    BitBucket: https://bitbucket.org/AlessioCali/  
    LinkedIn: https://www.linkedin.com/in/alessio-giuseppe-calì-599631132/  
* Ema Srdoc - Modeling and rendering  
    GitHub: https://github.com/ema-s/  
    BitBucket: https://bitbucket.org/ema_s/  
    LinkedIn: https://www.linkedin.com/in/ema-srdoc-93b44a87/  
    
## External contributors
* Eric Matyas - Background Music  
    Eric didn't contribute directly to this project, but his website offers a wide selection of free to use audio works! Check him out!  
    Personal site: http://soundimage.org/