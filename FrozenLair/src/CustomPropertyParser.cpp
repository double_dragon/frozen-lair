#include "CallbackParser.h"
#include "Console.h"
#include "CustomPropertyParser.h"
#include "FrozenLairApplication.h"
#include "Physics.h"

FL::CustomPropertyParser::CustomPropertyParser(FL::FrozenLairApplication *mApp) {
	this->mApp = mApp;
	handlers["solid"] = &FL::CustomPropertyParser::parseSolid;
	handlers["trigger"] = &FL::CustomPropertyParser::parseTrigger;
	handlers["castShadows"] = &FL::CustomPropertyParser::parseCastShadows;
	handlers["schedules"] = &FL::CustomPropertyParser::parseSchedules;
}

void FL::CustomPropertyParser::parseHierarchy(Ogre::Node * node) {
	Ogre::UserObjectBindings& properties = node->getUserObjectBindings();
	auto childIterator = node->getChildIterator();
	auto acceptedProperties = getAcceptedTypes();

	for (auto prop = acceptedProperties.begin(); prop != acceptedProperties.end(); ++prop) {
		Ogre::Any foundProp = properties.getUserAny(*prop);

		if (!foundProp.isEmpty()) {
			parseProperty(node, *prop, foundProp);
		}
	}

	childIterator.begin();
	while (childIterator.hasMoreElements()) {
		parseHierarchy(childIterator.getNext());
	}
}

void FL::CustomPropertyParser::parseProperty(Ogre::Node* node, Ogre::String name, Ogre::Any value) {
	void (FL::CustomPropertyParser::*handler)(Ogre::Node*, Ogre::Any);
	auto search = handlers.find(name);

	if (search != handlers.end()) {
		handler = search->second;
		(this->*handler)(node, value);
	}
}

bool FL::CustomPropertyParser::accepts(Ogre::String type) {
	return (std::find(acceptedTypes.begin(), acceptedTypes.end(), type) != acceptedTypes.end());
}

std::list<Ogre::String> FL::CustomPropertyParser::getAcceptedTypes() {
	return acceptedTypes;
}

void FL::CustomPropertyParser::parseSolid(Ogre::Node* node, Ogre::Any value) {
	FL::Logger *mConsole = mApp->getLogger();

	Ogre::UserObjectBindings& props = node->getUserObjectBindings();
	Ogre::SceneNode* sceneNode = static_cast<Ogre::SceneNode*>(node);
	Ogre::Vector3 pos = node->_getDerivedPosition();
	Ogre::Quaternion rot = node->_getDerivedOrientation();
	Ogre::Vector3 scale = node->_getDerivedScale();
	Ogre::Real mass = 0.0;
	Ogre::Real restitution = 0.0;
	Ogre::Real friction = 1.0;
	Ogre::Real damp = 0.0;
	Ogre::Vector3 halfExtent;

	btCollisionShape *shape;
	btTriangleMesh *mesh = nullptr;

	if (!props.getUserAny("mass").isEmpty()) {
		mass = Ogre::Real(props.getUserAny("mass").get<Ogre::Real>());
	}

	if (!props.getUserAny("restitution").isEmpty()) {
		restitution = Ogre::Real(props.getUserAny("restitution").get<Ogre::Real>());
	}

	if (!props.getUserAny("friction").isEmpty()) {
		friction = Ogre::Real(props.getUserAny("friction").get<Ogre::Real>());
	}

	if (!props.getUserAny("damp").isEmpty()) {
		damp = Ogre::Real(props.getUserAny("damp").get<Ogre::Real>());
	}

	Ogre::String type = value.get<Ogre::String>();
	if (type == "box") {
		shape = makeBoundingBox(node);
	}
	else if (type == "sphere") {
		shape = makeSphere(node);
	}
	else if (type == "mesh") {
		mass = 0.f;	// Mesh objects must be static
		shape = makeMesh(node, mesh);
	}
	else if (Ogre::StringUtil::startsWith(type, "capsule", false)) {
		Ogre::StringVector args = Ogre::StringUtil::split(type, " ");
		if (args.size() < 3) {
			mApp->getLogger()->log(FL::Logger::LOG_WARNING, "[Solid-Capsule Property] Height and/or radius unknown. Defaulting to sphere");
			shape = makeSphere(node);
		}
		else {
			Ogre::Real height = Ogre::StringConverter::parseReal(args[1]);
			Ogre::Real radius = Ogre::StringConverter::parseReal(args[2]);
			shape = makeCapsule(node, height, radius);
		}
	}
	else {
		shape = makeBoundingBox(node);
	}

	btTransform transform;
	transform.setIdentity();
	transform.setOrigin(btVector3(pos.x, pos.y, pos.z));
	transform.setRotation(btQuaternion(rot.x, rot.y, rot.z, rot.w));

	btVector3 inertia(0, 0, 0);
	btDefaultMotionState *motionState = new btDefaultMotionState(transform);
	if (mass != 0) { shape->calculateLocalInertia(mass, inertia); }

	btRigidBody::btRigidBodyConstructionInfo rbInfo(mass, motionState, shape, inertia);
	btRigidBody *body = new btRigidBody(rbInfo);
	body->setUserPointer(node);
	body->setRestitution(restitution);
	body->setFriction(friction);
	body->setDamping(damp, body->getLinearDamping());

	props.setUserAny("Body", Ogre::Any(body));
	mApp->getPhysics()->addBody(body, shape, mesh);
}

void FL::CustomPropertyParser::parseTrigger(Ogre::Node * node, Ogre::Any value) {
	FL::Logger *mLogger = mApp->getLogger();

	Ogre::UserObjectBindings& props = node->getUserObjectBindings();
	Ogre::SceneNode* sceneNode = static_cast<Ogre::SceneNode*>(node);
	Ogre::Vector3 pos = node->_getDerivedPosition();
	Ogre::Quaternion rot = node->_getDerivedOrientation();
	Ogre::Vector3 scale = node->_getDerivedScale();
	Ogre::Vector3 halfExtent;

	btCollisionShape *shape;

	if (mLogger) {
		mLogger->log(FL::Logger::LOG_INFO, "\\[" + node->getName() + "\\] : Property trigger set to " + value.get<Ogre::String>());
	}

	Ogre::String type = value.get<Ogre::String>();
	if (type == "box") {
		shape = makeBoundingBox(node);
	}
	else if (type == "sphere") {
		shape = makeSphere(node);
	}
	else {
		// Mesh type not supported for triggers.
		shape = makeBoundingBox(node);
	}

	btTransform transform;
	transform.setIdentity();
	transform.setOrigin(btVector3(pos.x, pos.y, pos.z));
	transform.setRotation(btQuaternion(rot.x, rot.y, rot.z, rot.w));

	btGhostObject *trigger = new btGhostObject();
	trigger->setCollisionShape(shape);
	trigger->setWorldTransform(transform);
	trigger->setUserPointer(node);

	props.setUserAny("TriggerBody", Ogre::Any(trigger));
	mApp->getPhysics()->addTrigger(trigger, shape);
}

void FL::CustomPropertyParser::parseCastShadows(Ogre::Node * node, Ogre::Any value) {
	Ogre::String strValue = value.get<Ogre::String>();
	bool bValue = strValue == "true" ? true : false;
	Ogre::SceneNode *asSceneNode = static_cast<Ogre::SceneNode*>(node);

	if (asSceneNode && asSceneNode->numAttachedObjects() > 0) {
		auto it = asSceneNode->getAttachedObjectIterator();
		for (auto el = it.begin(); el != it.end(); el++) {
			el->second->setCastShadows(bValue);
		}
	}
}

void FL::CustomPropertyParser::parseSchedules(Ogre::Node * node, Ogre::Any value) {
	Ogre::String arg = value.get<Ogre::String>();
	Ogre::StringVector args = Ogre::StringUtil::split(arg, " ", 2);

	if (args.size() == 3) {
		Ogre::Real duration = std::stof(args[0]);
		bool repeat = args[1] == "true" ? true : false;
		mApp->getCallbackParser()->schedule(node, args[2], duration, repeat);
	}
}

btCollisionShape * FL::CustomPropertyParser::makeBoundingBox(Ogre::Node * node) {
	Ogre::SceneNode *sceneNode = static_cast<Ogre::SceneNode*>(node);
	Ogre::Vector3 halfExtent;
	Ogre::Vector3 scale = node->_getDerivedScale();

	if (sceneNode && sceneNode->numAttachedObjects() > 0) {
		Ogre::Entity *entity = static_cast<Ogre::Entity*>(sceneNode->getAttachedObject(0));
		halfExtent = entity->getBoundingBox().getHalfSize();
		for (unsigned int i = 0; i < 3; i++) {
			if (halfExtent[i] == 0.0f) { halfExtent[i] = 0.025f; }	// Prevent 'flat' boxes
		}
	}
	else {
		halfExtent = Ogre::Vector3(1, 1, 1);	// Resort to default bounding box if no entity found
	}

	btBoxShape *shape = new btBoxShape(btVector3(halfExtent.x, halfExtent.y, halfExtent.z));
	shape->setLocalScaling(btVector3(scale.x, scale.y, scale.z));
	return shape;
}

btCollisionShape * FL::CustomPropertyParser::makeSphere(Ogre::Node * node) {
	Ogre::SceneNode *sceneNode = static_cast<Ogre::SceneNode*>(node);
	Ogre::Real radius = 2.f;
	Ogre::Vector3 scale = node->_getDerivedScale();

	if (sceneNode && sceneNode->numAttachedObjects() > 0) {
		Ogre::Entity *entity = static_cast<Ogre::Entity*>(sceneNode->getAttachedObject(0));
		radius = entity->getBoundingRadius();
	}
	
	btSphereShape *shape = new btSphereShape(radius);
	shape->setLocalScaling(btVector3(scale.x, scale.y, scale.z));
	return shape;
}

btCollisionShape *FL::CustomPropertyParser::makeMesh(Ogre::Node *node, btTriangleMesh* &mesh) {
	Ogre::SceneNode *sceneNode = static_cast<Ogre::SceneNode*>(node);
	Ogre::Entity *entity;
	Ogre::Vector3 scale = node->_getDerivedScale();

	if (node && sceneNode->numAttachedObjects() > 0) {
		entity = static_cast<Ogre::Entity*>(sceneNode->getAttachedObject(0));
	}
	else {
		OGRE_EXCEPT(Ogre::Exception::ERR_ITEM_NOT_FOUND, "Node does not own an entity.", "CustomPropertyParser::makeMesh");
	}

	const Ogre::MeshPtr& ogreMesh = entity->getMesh();

	size_t vertex_count;
	size_t index_count;
	Ogre::Vector3 *vertices;
	unsigned long int *indices;

	getMeshInformation(ogreMesh.getPointer(), vertex_count, vertices, index_count, indices);

	btTriangleMesh *meshInfo = new btTriangleMesh();
	meshInfo->preallocateVertices(vertex_count);
	meshInfo->preallocateIndices(index_count);

	if ((index_count % 3) != 0) {
		OGRE_EXCEPT(Ogre::Exception::ERR_INVALIDPARAMS, "Error while creating mesh collider: faces are not all triangles.", "CustomPropertyParser::makeMesh");
	}

	for (unsigned int i = 0; i < vertex_count; i++) {
		Ogre::Vector3 v = vertices[i];
		meshInfo->findOrAddVertex(btVector3(v.x, v.y, v.z), false);
	}
	for (unsigned int i = 0; i < index_count; i += 3) {
		meshInfo->addTriangleIndices(indices[i], indices[i + 1], indices[i + 2]);
	}

	btBvhTriangleMeshShape *meshShape = new btBvhTriangleMeshShape(meshInfo, true);
	meshShape->setLocalScaling(btVector3(scale.x, scale.y, scale.z));
	delete[] vertices;
	delete[] indices;

	mesh = meshInfo;
	return meshShape;
}

btCollisionShape * FL::CustomPropertyParser::makeCapsule(Ogre::Node * node, Ogre::Real height, Ogre::Real radius) {
	Ogre::Vector3 scale = node->_getDerivedScale();
	btCapsuleShape *shape = new btCapsuleShape(radius, height);
	shape->setLocalScaling(btVector3(scale.x, scale.y, scale.z));
	return shape;
}

/*
	Source for this function:
	http://www.ogre3d.org/tikiwiki/RetrieveVertexData&structure=Cookbook
*/
void FL::CustomPropertyParser::getMeshInformation(
	const Ogre::Mesh* const mesh,
	size_t &vertex_count,
	Ogre::Vector3* &vertices,
	size_t &index_count,
	unsigned long* &indices,
	const Ogre::Vector3 &position,
	const Ogre::Quaternion &orient,
	const Ogre::Vector3 &scale
)
{
	bool added_shared = false;
	size_t current_offset = 0;
	size_t shared_offset = 0;
	size_t next_offset = 0;
	size_t index_offset = 0;

	vertex_count = index_count = 0;

	// Calculate how many vertices and indices we're going to need
	for (unsigned short i = 0; i < mesh->getNumSubMeshes(); ++i)
	{
		Ogre::SubMesh* submesh = mesh->getSubMesh(i);
		// We only need to add the shared vertices once
		if (submesh->useSharedVertices)
		{
			if (!added_shared)
			{
				vertex_count += mesh->sharedVertexData->vertexCount;
				added_shared = true;
			}
		}
		else
		{
			vertex_count += submesh->vertexData->vertexCount;
		}
		// Add the indices
		index_count += submesh->indexData->indexCount;
	}

	// Allocate space for the vertices and indices
	vertices = new Ogre::Vector3[vertex_count];
	indices = new unsigned long[index_count];

	added_shared = false;

	// Run through the submeshes again, adding the data into the arrays
	for (unsigned short i = 0; i < mesh->getNumSubMeshes(); ++i)
	{
		Ogre::SubMesh* submesh = mesh->getSubMesh(i);

		Ogre::VertexData* vertex_data = submesh->useSharedVertices ? mesh->sharedVertexData : submesh->vertexData;

		if ((!submesh->useSharedVertices) || (submesh->useSharedVertices && !added_shared))
		{
			if (submesh->useSharedVertices)
			{
				added_shared = true;
				shared_offset = current_offset;
			}

			const Ogre::VertexElement* posElem =
				vertex_data->vertexDeclaration->findElementBySemantic(Ogre::VES_POSITION);

			Ogre::HardwareVertexBufferSharedPtr vbuf =
				vertex_data->vertexBufferBinding->getBuffer(posElem->getSource());

			unsigned char* vertex =
				static_cast<unsigned char*>(vbuf->lock(Ogre::HardwareBuffer::HBL_READ_ONLY));

			// There is _no_ baseVertexPointerToElement() which takes an Ogre::Real or a double
			//  as second argument. So make it float, to avoid trouble when Ogre::Real will
			//  be comiled/typedefed as double:
			//Ogre::Real* pReal;
			float* pReal;

			for (size_t j = 0; j < vertex_data->vertexCount; ++j, vertex += vbuf->getVertexSize())
			{
				posElem->baseVertexPointerToElement(vertex, &pReal);
				Ogre::Vector3 pt(pReal[0], pReal[1], pReal[2]);
				vertices[current_offset + j] = (orient * (pt * scale)) + position;
			}

			vbuf->unlock();
			next_offset += vertex_data->vertexCount;
		}

		Ogre::IndexData* index_data = submesh->indexData;
		size_t numTris = index_data->indexCount / 3;
		Ogre::HardwareIndexBufferSharedPtr ibuf = index_data->indexBuffer;

		bool use32bitindexes = (ibuf->getType() == Ogre::HardwareIndexBuffer::IT_32BIT);

		unsigned long* pLong = static_cast<unsigned long*>(ibuf->lock(Ogre::HardwareBuffer::HBL_READ_ONLY));
		unsigned short* pShort = reinterpret_cast<unsigned short*>(pLong);

		size_t offset = (submesh->useSharedVertices) ? shared_offset : current_offset;

		if (use32bitindexes)
		{
			for (size_t k = 0; k < numTris * 3; ++k)
			{
				indices[index_offset++] = pLong[k] + static_cast<unsigned long>(offset);
			}
		}
		else
		{
			for (size_t k = 0; k < numTris * 3; ++k)
			{
				indices[index_offset++] = static_cast<unsigned long>(pShort[k]) +
					static_cast<unsigned long>(offset);
			}
		}

		ibuf->unlock();
		current_offset = next_offset;
	}
}