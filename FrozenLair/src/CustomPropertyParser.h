#pragma once

#include <map>

#include <Ogre.h>

#include <BulletCollision\CollisionDispatch\btGhostObject.h>
#include <BulletCollision\CollisionShapes\btTriangleMesh.h>

namespace FL {
	class FrozenLairApplication;

	class CustomPropertyParser {
	public:
		CustomPropertyParser(FL::FrozenLairApplication* mApp);

		void parseHierarchy(Ogre::Node* node);
		void parseProperty(Ogre::Node* node, Ogre::String name, Ogre::Any value);
		bool accepts(Ogre::String type);
		std::list<Ogre::String> getAcceptedTypes();
		
	private:
		std::map<Ogre::String, void(FL::CustomPropertyParser::*)(Ogre::Node*, Ogre::Any)> handlers;
		std::list<Ogre::String> acceptedTypes = {
			"solid",
			"trigger",
			"castShadows",
			"schedules"
		};

		FL::FrozenLairApplication* mApp;

		void parseSolid(Ogre::Node* node, Ogre::Any value);
		void parseTrigger(Ogre::Node* node, Ogre::Any value);
		void parseCastShadows(Ogre::Node* node, Ogre::Any value);
		void parseSchedules(Ogre::Node *node, Ogre::Any value);

		btCollisionShape *makeBoundingBox(Ogre::Node *node);
		btCollisionShape *makeSphere(Ogre::Node *node);
		btCollisionShape *makeMesh(Ogre::Node *node, btTriangleMesh* &mesh);
		btCollisionShape *makeCapsule(Ogre::Node * node, Ogre::Real height, Ogre::Real radius);

		void getMeshInformation(
			const Ogre::Mesh* const mesh,
			size_t &vertex_count,
			Ogre::Vector3* &vertices,
			size_t &index_count,
			unsigned long* &indices,
			const Ogre::Vector3 &position = Ogre::Vector3::ZERO,
			const Ogre::Quaternion &orient = Ogre::Quaternion::IDENTITY,
			const Ogre::Vector3 &scale = Ogre::Vector3::UNIT_SCALE
		);
	};

}