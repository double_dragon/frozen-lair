#pragma once

#include <stdexcpt.h>

// Disable annoying bullet warnings
#pragma warning(push)
#pragma warning(disable : 4305)
#include <btBulletDynamicsCommon.h>
#pragma warning(pop)

#include <OgreCamera.h>
#include <OgreSceneNode.h>

namespace FL {
	class FrozenLairApplication;

	class Player
	{
	public:
		Player(FL::FrozenLairApplication *mApp, Ogre::Camera* mCamera, btRigidBody* pBody);
		~Player();

		void moveForward(bool forward);
		void moveSidewise(bool right);
		void rotate(Ogre::Radian yaw, Ogre::Radian pitch);
		void stop(bool side, bool forward);
		void update();

		bool getFlag(Ogre::String flagName);
		void setFlag(Ogre::String flagName, bool value);

		void throwBone();
		void addBones(unsigned int count);
		int getBoneCount();

		void getRayCastEnds(btVector3& start, btVector3& end);
		void setSpeed(Ogre::Real speed);

	private:
		const Ogre::Radian MAX_PITCH = Ogre::Radian(Ogre::Math::DegreesToRadians(60.0));
		const Ogre::Radian MIN_PITCH = Ogre::Radian(Ogre::Math::DegreesToRadians(-70.0));
		const Ogre::Real RAYCAST_LENGTH = Ogre::Real(60.0f);
		const Ogre::Real PLAYER_GRAVITY = Ogre::Real(300.f);

		const Ogre::Real BONE_OFFSET = 0.75f;
		const Ogre::Radian BONE_PITCH = Ogre::Radian(Ogre::Math::DegreesToRadians(30.f));
		const Ogre::Real BONE_ANGULAR_FORCE = 25.f;
		const Ogre::Real BONE_LAUNCH_FORCE = 25.f;

		std::map<Ogre::String, bool> mFlags = { 
			{"fReadJournal", false}, 
			{"fDestroyedBarrier", false}, 
			{"fHasCheckedTreasure", false},
			{"fHasLockpick", false},
			{"fHasTreasure", false}
		};

		Ogre::Real speed = 70.f;

		Ogre::Radian yaw, pitch;
		Ogre::Camera *fpCamera;
		FL::FrozenLairApplication *mApp;
		btRigidBody* pBody;
		bool right, forward;
		bool movingSide, movingForward;
		unsigned int boneCounter;
	};
}