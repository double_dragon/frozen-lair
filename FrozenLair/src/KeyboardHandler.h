#pragma once

#include <stack>

#include <OISKeyboard.h>

#include "InputCommons.h"

namespace FL {
	class FrozenLairApplication;

	namespace Input {
		class KeyboardHandler : public OIS::KeyListener
		{
		public:
			KeyboardHandler(FL::FrozenLairApplication* app);

			void setState(FL::Input::InputState state);
			void revertState();

			virtual bool keyPressed(const OIS::KeyEvent& key);
			virtual bool keyReleased(const OIS::KeyEvent& key);
		private:
			FL::FrozenLairApplication *mApp = nullptr;
			std::stack<FL::Input::InputState> stateStack;
			FL::Input::InputState inputState;

			const Ogre::Real ROTATE_SCALE = Ogre::Real(0.1);

			bool keyPressedDefault(const OIS::KeyEvent& key);
			bool keyPressedCEGUI(const OIS::KeyEvent& key);
			bool keyPressedOutro(const OIS::KeyEvent& key);

			bool keyReleasedDefault(const OIS::KeyEvent& key);
			bool keyReleasedCEGUI(const OIS::KeyEvent& key);
			bool keyReleasedOutro(const OIS::KeyEvent& key);
		};
	}
}