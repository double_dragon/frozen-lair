#include "AudioManager.h"
#include "CallbackParser.h"
#include "Console.h"
#include "CustomPropertyParser.h"
#include "DialogManager.h"
#include "FrozenLairApplication.h"
#include "GUIManager.h"
#include "KeyboardHandler.h"
#include "LockpickGame.h"
#include "MouseHandler.h"
#include "Physics.h"
#include "Player.h"
#include "ShaderManager.h"

FL::FrozenLairApplication::FrozenLairApplication() :
	mRoot(0),
	mResourceCfg(Ogre::StringUtil::BLANK),
	mPluginsCfg(Ogre::StringUtil::BLANK)
{ 
}

FL::FrozenLairApplication::~FrozenLairApplication() {
	if (mWindow) {
		Ogre::WindowEventUtilities::removeWindowEventListener(mWindow, this);
		windowClosed(mWindow);
	}

	if (mGuiRenderer) { CEGUI::OgreRenderer::destroySystem(); }

	if (mAudioManager) { delete mAudioManager; }
	if (mKeyboardListener) { delete mKeyboardListener; }
	if (mMouseListener) { delete mMouseListener; }
	if (mPropertyParser) { delete mPropertyParser;  }
	if (mCallbackParser) { delete mCallbackParser; }
	if (mDialogManager) { delete mDialogManager; }
	if (mGUIManager) { delete mGUIManager; }
	if (mLockpickGame) { delete mLockpickGame; }
	if (mConsole) { delete mConsole; }
	if (mPhysics) { delete mPhysics; }
	if (mPlayer) { delete mPlayer;  }
	if (mShaderManager) { delete mShaderManager; }
	if (mRoot) { delete mRoot; }
}

bool FL::FrozenLairApplication::startup() {
	// Create root object and load resource and plugin configurations
	mRoot = new Ogre::Root(mPluginsCfg);
	Ogre::ConfigFile cf;
	cf.load(mResourceCfg);

	Ogre::String name, locType;
	Ogre::ConfigFile::SectionIterator secIt = cf.getSectionIterator();

	while (secIt.hasMoreElements()) {
		Ogre::String groupName = secIt.peekNextKey();
		Ogre::ConfigFile::SettingsMultiMap* settings = secIt.getNext();
		Ogre::ConfigFile::SettingsMultiMap::iterator it;

		for (it = settings->begin(); it != settings->end(); ++it) {
			locType = it->first;
			name = it->second;
			Ogre::ResourceGroupManager::getSingleton().addResourceLocation(name, locType, groupName);
		}
	}

	// Setup window system
	if (!mRoot->showConfigDialog()) { return false; }
	mWindow = mRoot->initialise(true, "FrozenLair");

	return true;
}

void FL::FrozenLairApplication::loadScene() {
	// Initialize resources
	Ogre::TextureManager::getSingleton().setDefaultNumMipmaps(5);
	Ogre::ResourceGroupManager::getSingleton().initialiseAllResourceGroups();
	Ogre::Math::setAngleUnit(Ogre::Math::AngleUnit::AU_RADIAN);

	mSceneMgr = mRoot->createSceneManager(Ogre::ST_GENERIC);
	mSceneMgr->setAmbientLight(Ogre::ColourValue::White * 0.4f);
	mSceneMgr->setShadowTechnique(Ogre::SHADOWTYPE_TEXTURE_ADDITIVE_INTEGRATED);
	mSceneMgr->setShadowTextureSize(1024);
	mSceneMgr->setShadowTextureCount(2);

	// Load .scene file
	Ogre::dsi::DotSceneInfo* infoPtr = new Ogre::dsi::DotSceneInfo;
	Ogre::dsi::DotSceneProcessor* loader = new Ogre::dsi::DotSceneProcessor;

	loader->initialize();
	loader->load(
		"FrozenLair.blend.scene",
		mSceneMgr,
		mWindow,
		"General",
		"",
		mSceneMgr->getRootSceneNode(),
		true,
		true,
		&infoPtr
	);
	loader->shutdown();

	delete loader;
	delete infoPtr;

	// Setup aspect ratio and viewport
	mCamera = mSceneMgr->getCamera("Camera");

	Ogre::Viewport* vp = mWindow->addViewport(mCamera);
	vp->setBackgroundColour(Ogre::ColourValue::Black);

	mCamera->setAspectRatio(
		Ogre::Real(vp->getActualWidth()) /
		Ogre::Real(vp->getActualHeight())
	);
}

void FL::FrozenLairApplication::startOIS() {
	// Setup OIS
	OIS::ParamList pl;
	size_t windowHnd = 0;
	std::ostringstream windowHndStr;

	mWindow->getCustomAttribute("WINDOW", &windowHnd);
	windowHndStr << windowHnd;
	pl.insert(std::make_pair<std::string, std::string>(std::string("WINDOW"), windowHndStr.str()));

	mInputManager = OIS::InputManager::createInputSystem(pl);
	mKeyboard = static_cast<OIS::Keyboard*>(mInputManager->createInputObject(OIS::OISKeyboard, true));
	mMouse = static_cast<OIS::Mouse*>(mInputManager->createInputObject(OIS::OISMouse, true));

	mKeyboardListener = new Input::KeyboardHandler(this);
	mKeyboard->setEventCallback(mKeyboardListener);

	mMouseListener = new Input::MouseHandler(this);
	mMouse->setEventCallback(mMouseListener);

	windowResized(mWindow);
	Ogre::WindowEventUtilities::addWindowEventListener(mWindow, this);
}

void FL::FrozenLairApplication::startCEGUI() {
	// Setup GUI
	CEGUI::Font::setDefaultResourceGroup("Fonts");
	CEGUI::Scheme::setDefaultResourceGroup("Schemes");
	CEGUI::WindowManager::setDefaultResourceGroup("Layouts");
	CEGUI::WidgetLookManager::setDefaultResourceGroup("Looknfeel");
	CEGUI::ImageManager::setImagesetDefaultResourceGroup("Imagesets");

	mGuiRenderer = &CEGUI::OgreRenderer::bootstrapSystem();

	CEGUI::SchemeManager::getSingleton().createFromFile("Generic.scheme");
	CEGUI::SchemeManager::getSingleton().createFromFile("FrozenLair.scheme");
	CEGUI::System::getSingleton().getDefaultGUIContext().setDefaultFont("Batang-12");
	CEGUI::System::getSingleton().getDefaultGUIContext().getMouseCursor().setDefaultImage("FrozenLair/CursorWhite");

	mRootWindow = CEGUI::WindowManager::getSingleton().loadLayoutFromFile("FrozenLair.layout");
	CEGUI::System::getSingleton().getDefaultGUIContext().setRootWindow(mRootWindow);

	mConsole = new FL::Console(mRootWindow->getChild("Console"), this);
}

void FL::FrozenLairApplication::initialiseGame() {
	// Setup physics
	mCallbackParser = new FL::CallbackParser(this);
	mPhysics = new FL::Physics(this, mSceneMgr);

	// Setup custom properties
	mPropertyParser = new FL::CustomPropertyParser(this);
	mPropertyParser->parseHierarchy(mSceneMgr->getRootSceneNode());

	// Setup game elements
	Ogre::SceneNode* playerNode = mSceneMgr->getSceneNode("Player");
	btRigidBody* playerBody = playerNode->getUserObjectBindings().getUserAny("Body").get<btRigidBody*>();
	playerNode->setVisible(false);
	mPlayer = new FL::Player(this, mCamera, playerBody);
	mLockpickGame = new FL::LockpickGame(this, mRootWindow->getChild("LockpickWindow"));

	// Load dialogs
	mDialogManager = new DialogManager(mRootWindow->getChild("DialogContainer"), this);
	mDialogManager->process("dialogs.xml");

	// Load GUI manager
	mGUIManager = new GUIManager(this, mRootWindow);

	// Start Audio
	mAudioManager = new FL::AudioManager(this);

	// Init shader manager
	mShaderManager = new FL::ShaderManager(this);
}

void FL::FrozenLairApplication::deleteObjects() {
	while (deletionStack.size()) {
		Ogre::SceneNode *node = deletionStack.back();
		deletionStack.pop_back();

		if (!node->getUserObjectBindings().getUserAny("Body").isEmpty()) {
			btRigidBody *obj = node->getUserObjectBindings().getUserAny("Body").get<btRigidBody*>();
			btCollisionShape *shape = obj->getCollisionShape();
			if (obj && shape) {
				mPhysics->remove(obj, shape);
			}
		}
		if (!node->getUserObjectBindings().getUserAny("TriggerBody").isEmpty()) {
			btGhostObject *obj = node->getUserObjectBindings().getUserAny("TriggerBody").get<btGhostObject*>();
			btCollisionShape *shape = obj->getCollisionShape();
			if (obj && shape) {
				mPhysics->remove(obj, shape);
			}
		}

		node->removeAndDestroyAllChildren();
		mSceneMgr->destroySceneNode(node);
	}
}

bool FL::FrozenLairApplication::go() {
	// Use different configurations for debug and release
#ifdef _DEBUG
	mResourceCfg = "resources_d.cfg";
	mPluginsCfg = "plugins_d.cfg";
#else
	mResourceCfg = "resources.cfg";
	mPluginsCfg = "plugins.cfg";
#endif

	if (!startup()) { return false; }
	loadScene();
	startOIS();
	startCEGUI();
	initialiseGame();
	
	// Start rendering loop
	mRoot->addFrameListener(this);
	mAudioManager->startBGM();
	mRoot->startRendering();	

	return true;
}

void FL::FrozenLairApplication::stop() {
	running = false;
}

void FL::FrozenLairApplication::startGame() {
	mKeyboardListener->setState(FL::Input::IS_DEFAULT);
	mMouseListener->setState(FL::Input::IS_DEFAULT);
	mCallbackParser->parse(nullptr, "dialog Intro");
}

void FL::FrozenLairApplication::startOutro() {
	mKeyboardListener->setState(FL::Input::IS_OUTRO);
	mMouseListener->setState(FL::Input::IS_OUTRO);
	mGUIManager->startOutro();
}

void FL::FrozenLairApplication::destroyObject(Ogre::SceneNode * node) {
	deletionStack.push_back(node);
}

FL::AudioManager * FL::FrozenLairApplication::getAudioManager() {
	return mAudioManager;
}

FL::Player * FL::FrozenLairApplication::getPlayer() {
	return mPlayer;
}

FL::Logger * FL::FrozenLairApplication::getLogger() {
	return mConsole;
}

FL::Console * FL::FrozenLairApplication::getConsole() {
	return mConsole;
}

void FL::FrozenLairApplication::windowFocusChange(Ogre::RenderWindow * rw) {
	if (mPlayer) { mPlayer->stop(true, true); }
}

bool FL::FrozenLairApplication::frameStarted(const Ogre::FrameEvent & fe) {
	if (mWindow->isClosed()) { return false; }

	return running;
}

bool FL::FrozenLairApplication::frameRenderingQueued(const Ogre::FrameEvent & fe) {
	if (mWindow->isClosed()) { return false; }

	mKeyboard->capture();
	mMouse->capture();

	CEGUI::System::getSingleton().injectTimePulse(fe.timeSinceLastFrame);

	return running;
}

bool FL::FrozenLairApplication::frameEnded(const Ogre::FrameEvent & fe) {
	mPhysics->update(fe.timeSinceLastFrame);
	mPlayer->update();
	mGUIManager->update(fe.timeSinceLastFrame);
	mCallbackParser->update(fe.timeSinceLastFrame);
	mLockpickGame->update(fe.timeSinceLastFrame);
	deleteObjects();

	return running;
}

FL::Physics *FL::FrozenLairApplication::getPhysics() {
	return mPhysics;
}

FL::DialogManager * FL::FrozenLairApplication::getDialogManager() {
	return mDialogManager;
}

FL::LockpickGame * FL::FrozenLairApplication::getLockpickGame() {
	return mLockpickGame;
}

FL::CallbackParser * FL::FrozenLairApplication::getCallbackParser() {
	return mCallbackParser;
}

FL::CustomPropertyParser * FL::FrozenLairApplication::getPropertyParser() {
	return mPropertyParser;
}

FL::Input::KeyboardHandler * FL::FrozenLairApplication::getKeyboardHandler() {
	return mKeyboardListener;
}

FL::Input::MouseHandler * FL::FrozenLairApplication::getMouseHandler() {
	return mMouseListener;
}

FL::GUIManager * FL::FrozenLairApplication::getGUIManager() {
	return mGUIManager;
}

FL::ShaderManager * FL::FrozenLairApplication::getShaderManager() {
	return mShaderManager;
}

Ogre::SceneManager * FL::FrozenLairApplication::getSceneManager() {
	return mSceneMgr;
}

void FL::FrozenLairApplication::windowResized(Ogre::RenderWindow * rw) {
	unsigned int width, height, depth;
	int left, top;
	rw->getMetrics(width, height, depth, left, top);

	const OIS::MouseState &ms = mMouse->getMouseState();
	ms.width = width;
	ms.height = height;
}

void FL::FrozenLairApplication::windowClosed(Ogre::RenderWindow* rw) {
	if (rw == mWindow) {
		if (mInputManager) {
			mInputManager->destroyInputObject(mMouse);
			mInputManager->destroyInputObject(mKeyboard);
			
			OIS::InputManager::destroyInputSystem(mInputManager);
			mInputManager = 0;
		}
	}
}

/* -------------- MAIN DOWN HERE --------------- */

#if OGRE_PLATFORM == OGRE_PLATFORM_WIN32
#define WIN32_LEAN_AND_MEAN
#include "windows.h"
#endif

#ifdef __cplusplus
extern "C" {
#endif

#if OGRE_PLATFORM == OGRE_PLATFORM_WIN32
	INT WINAPI WinMain(HINSTANCE hInst, HINSTANCE, LPSTR strCmdLine, INT)
#else
	int main(int argc, char *argv[])
#endif
	{
		// Create application object
		FL::FrozenLairApplication app;

		try {
			app.go();
		}
		catch (Ogre::Exception& e) {
#if OGRE_PLATFORM == OGRE_PLATFORM_WIN32
			MessageBox(NULL, e.getFullDescription().c_str(), "An exception has occurred!", MB_OK | MB_ICONERROR | MB_TASKMODAL);
#else
			std::cerr << "An exception has occurred: " <<
				e.getFullDescription().c_str() << std::endl;
#endif
		}

		return 0;
	}

#ifdef __cplusplus
}
#endif
