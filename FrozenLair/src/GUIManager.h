#pragma once

#include <Ogre.h>

#include <CEGUI\CEGUI.h>

namespace FL {
	class FrozenLairApplication;

	class GUIManager {
	public:
		GUIManager(FL::FrozenLairApplication *mApp, CEGUI::Window *mGUIRoot);

		void update(Ogre::Real delta);
		void showHint(Ogre::String text, Ogre::String icon);
		void hideHint();
		void startOutro();

	private:
		enum GUIState {
			GS_INTRO,
			GS_GAME,
			GS_OUTRO
		};

		const float FADE_SPEED;

		GUIState cState;
		FL::FrozenLairApplication *mApp;
		CEGUI::Window *mGUIRoot;

		void setShowBoneHint(bool show);
		void updateIntro(Ogre::Real delta);
		void updateGame(Ogre::Real delta);
		void updateOutro(Ogre::Real delta);
	};
}