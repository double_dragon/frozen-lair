#pragma once

#include <Ogre.h>
#include <CEGUI\CEGUI.h>

#include <XNPPrerequisites.h>
#include <tinyxml.h>

namespace FL {
	class FrozenLairApplication;

	class DialogManager {
	public:
		DialogManager(CEGUI::Window *dialogWindow, FL::FrozenLairApplication *mApp);
		~DialogManager();

		void process(const Ogre::String & resourceName);
		bool step(Ogre::String dialogId = Ogre::StringUtil::BLANK);

	protected:
		
		class Dialog {
		public:
			Dialog(Ogre::String id, Ogre::String onDialogEnd);

			Ogre::String next(Ogre::String &img);
			Ogre::String getOnDialogEnd();
			void push(Ogre::String pieceText, Ogre::String pieceImg);
			void reset();

		protected:
			typedef struct DialogPiece {
				Ogre::String text;
				Ogre::String img;
			} DialogPiece;

		private:
			unsigned int idx;
			Ogre::String id;
			Ogre::String onDialogEnd;

			std::vector<DialogPiece> pieces;
		};

	private:
		FL::FrozenLairApplication *mApp;
		std::map<Ogre::String, Dialog*> dialogMap;
		std::vector<Dialog*> dialogs;
		CEGUI::Window *dialogContainer = nullptr;
		Dialog *currentDialog = nullptr;

		void close();
	};
}