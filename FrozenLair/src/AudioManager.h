#pragma once

#include <SFML\Audio.hpp>

namespace FL {
	class FrozenLairApplication;
	class InputStream;

	/**
	 * This class handles reproducing sound effects and background music using SFML.
	 * Sound effects are loaded and cached when needed, while a single BGM istance is
	 * kept.
	 */
	class AudioManager {
	public:
		AudioManager(FL::FrozenLairApplication *mApp);
		~AudioManager();

		/** Starts the current BGM instance. As of now, its source is hardcoded. */
		void startBGM();

		/** Stops the current BGM instance. */
		void stopBGM();

		/**
		 * Plays a sound effect. The SFX is loaded and cached if not already, otherwise the cached version
		 * is used.
		 * @param sfxName the sound effect's name
		 * @param origin specifies where the sound originates from for directional audio. Has no effect
		 * if the playing track is not mono.
		 * @param loop if true, the sound effect is looped.
		 */
		void playSfx(Ogre::String sfxName, Ogre::Vector3 origin = Ogre::Vector3::ZERO, bool loop = false);

		/** Stops a sound effect playing */
		void stopSfx(Ogre::String sfxName);

		/** Pauses a sound effect playing */
		void pauseSfx(Ogre::String sfxName);

	private:
		FL::FrozenLairApplication *mApp;

		sf::Music mBgm;
		FL::InputStream *mBgmInputStream;

		std::map<Ogre::String, sf::Sound> sfxs;

		/**
		 * Loads a sound effect using Ogre's resource manager, and instantiates an SFML Sound for it.
		 * @param sfxName the sound effect's file name.
		 * @see FL::InputStream
		 */
		bool loadSfx(Ogre::String sfxName);
	};
}