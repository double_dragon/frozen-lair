#include "FrozenLairApplication.h"
#include "Logger.h"
#include "ShaderManager.h"

FL::ShaderManager::ShaderManager(FL::FrozenLairApplication * app) {
	this->mApp = app;
}

void FL::ShaderManager::startRipples(Ogre::SceneNode *node) {
	if (node->numAttachedObjects() > 0) {
		Ogre::Entity *entity = static_cast<Ogre::Entity*>(node->getAttachedObject(0));
		Ogre::MaterialPtr mat = entity->getSubEntity(0)->getMaterial();
		Ogre::Pass *pass = mat->getBestTechnique()->getPass("water_ripples");
		if (pass) {
			Ogre::GpuProgramParametersSharedPtr params = pass->getVertexProgramParameters();
			unsigned int curTimeIndex = params->getConstantDefinition("current_time").physicalIndex;
			float curTime = params->getFloatConstantList()[curTimeIndex];
			params->setNamedConstant("start_time", curTime);
		}
		else {
			mApp->getLogger()->log(FL::Logger::LOG_ERROR, "Can't reset ripple shader, object has not correct material");
		}
	}
}
