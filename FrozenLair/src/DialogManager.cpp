#include "CallbackParser.h"
#include "DialogManager.h"
#include "FrozenLairApplication.h"
#include "KeyboardHandler.h"
#include "MouseHandler.h"

FL::DialogManager::DialogManager(CEGUI::Window *dialogContainer, FL::FrozenLairApplication *mApp) {
	this->dialogContainer = dialogContainer;
	this->mApp = mApp;
}

FL::DialogManager::~DialogManager() {
	for (unsigned int i = 0; i < dialogs.size(); i++) {
		if (dialogs[i]) { delete dialogs[i]; }
	}
}

void FL::DialogManager::process(const Ogre::String &resourceName) {
	TiXmlDocument* doc = new TiXmlDocument();
	Ogre::DataStreamPtr data = Ogre::ResourceGroupManager::getSingleton().openResource(resourceName, "General");
	if (data->size()) {
		Ogre::String asString = data->getAsString() + '\0';
		doc->Parse(asString.c_str());
		if (doc->Error()) {
			OGRE_EXCEPT(Ogre::Exception::ERR_INVALIDPARAMS, doc->ErrorDesc(), "DialogManager::process");
		}
		else {
			// Root: <dialogs> element
			TiXmlElement *root = doc->RootElement();

			for (TiXmlElement *child = root->FirstChildElement() ; child; child = child->NextSiblingElement()) {
				// <dialog>
				const char *id = child->Attribute("id");
				const char *onDialogEnd = child->Attribute("onDialogEnd");
				if (!onDialogEnd) { onDialogEnd = ""; }
				if (!id) {
					// TODO Log warning
					continue;
				}

				Ogre::String oid = Ogre::String(id);
				if (dialogMap.count(oid)) {
					// TODO Log warning - non unique id
					continue;
				}
				Dialog *dialog = new Dialog(oid, onDialogEnd);

				dialogMap[oid] = dialog;
				dialogs.push_back(dialog);

				for (TiXmlElement *pieceElement = child->FirstChildElement(); pieceElement ; pieceElement = pieceElement->NextSiblingElement()) {
					// <dialog_piece>
					const char *pieceText = pieceElement->GetText();
					const char *pieceImg = pieceElement->Attribute("img");
					if (!pieceImg) { pieceImg = ""; }
					dialog->push(pieceText, pieceImg);
				}
			}
		}
	}
	else {
		OGRE_EXCEPT(Ogre::Exception::ERR_INVALIDPARAMS, doc->ErrorDesc(), "DialogManager::process");
	}
}

bool FL::DialogManager::step(Ogre::String dialogId) {
	if (dialogId != Ogre::StringUtil::BLANK && dialogMap.count(dialogId)) {
		if (currentDialog) { close(); }
		currentDialog = dialogMap[dialogId];
		currentDialog->reset();
	}
	if (currentDialog) {
		Ogre::String img;
		Ogre::String s = currentDialog->next(img);
		if (s != Ogre::StringUtil::BLANK) {
			CEGUI::Window *dialogText = dialogContainer->getChild("DialogWindow/DialogText");
			CEGUI::Window *dialogImg = dialogContainer->getChild("DialogImage");
			dialogText->setText(s);
			dialogContainer->setVisible(true);
			if (img.size()) {
				Ogre::StringVector imgParams = Ogre::StringUtil::split(img, ";", 2);
				dialogImg->setProperty("Image", imgParams[0]);
				dialogImg->setVisible(true);
				if (imgParams.size() >= 1) { dialogImg->setProperty("Position", imgParams[1]); }
				if (imgParams.size() >= 2) { dialogImg->setProperty("Size", imgParams[2]); }
			}
			else { dialogImg->setVisible(false); }

			return true;
		}
		else {
			close();
			return false;
		}
	}
	return false;
}

void FL::DialogManager::close() {
	dialogContainer->setVisible(false);
	Ogre::String onDialogEnd;

	if (currentDialog) {
		onDialogEnd = currentDialog->getOnDialogEnd();
		currentDialog = nullptr;
	}

	mApp->getKeyboardHandler()->revertState();
	mApp->getMouseHandler()->revertState();

	if (onDialogEnd.size()) {
		mApp->getCallbackParser()->parse(nullptr, onDialogEnd);
	}
}

FL::DialogManager::Dialog::Dialog(Ogre::String id, Ogre::String onDialogEnd) : idx(0) {
	this->id = id;
	this->onDialogEnd = onDialogEnd;
}

Ogre::String FL::DialogManager::Dialog::next(Ogre::String &img) {
	if (idx < pieces.size()) {
		DialogPiece dp = pieces[idx++];
		img = dp.img;
		return dp.text;
	}
	else {
		return Ogre::StringUtil::BLANK;
	}
}

Ogre::String FL::DialogManager::Dialog::getOnDialogEnd() {
	return onDialogEnd;
}

void FL::DialogManager::Dialog::push(Ogre::String pieceText, Ogre::String pieceImg) {
	DialogPiece dp;
	dp.text = pieceText;
	dp.img = pieceImg;
	pieces.push_back(dp);
}

void FL::DialogManager::Dialog::reset() {
	idx = 0;
}
