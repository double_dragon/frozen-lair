#include <SFML\Audio\Listener.hpp>
#include <string>

#include "AudioManager.h"
#include "Console.h"
#include "CustomPropertyParser.h"
#include "FrozenLairApplication.h"
#include "Player.h"

FL::Player::Player(FL::FrozenLairApplication *mApp, Ogre::Camera* mCamera, btRigidBody* pBody) : yaw(0), pitch(0), movingSide(false), movingForward(false), boneCounter(0) {
	if (!mCamera || !pBody) { throw std::invalid_argument("Camera and player can't be null"); }
	this->mApp = mApp;
	this->fpCamera = mCamera;
	this->pBody = pBody;
	this->pBody->setAngularFactor(0);
	this->pBody->setGravity(btVector3(0.f, -PLAYER_GRAVITY, 0.f));
	this->pBody->setRestitution(0.0f);
	this->pBody->setDamping(0.4f, 1.0f);

	Ogre::Vector3 pos = fpCamera->getRealPosition();
	sf::Listener::setPosition(sf::Vector3f(pos.x, pos.y, pos.z));
}

FL::Player::~Player() {
}

void FL::Player::moveForward(bool forward) {
	this->forward = forward;
	movingForward = true;
}

void FL::Player::moveSidewise(bool right) {
	this->right = right;
	movingSide = true;
}

void FL::Player::stop(bool side, bool forward) {
	if (side) { movingSide = false; }
	if (forward) { movingForward = false;  }
}

void FL::Player::throwBone() {
	if (boneCounter > 0) {
		boneCounter--;

		Ogre::SceneManager *mSceneMgr = mApp->getSceneManager();
		Ogre::SceneNode *entityNode = mSceneMgr->getRootSceneNode()->createChildSceneNode();
		Ogre::Entity *boneEntity = mSceneMgr->createEntity("Bone.mesh");
		Ogre::Vector3 bonePosition = fpCamera->getRealPosition() + fpCamera->getRealRight() * BONE_OFFSET;
		Ogre::Vector3 launchDirection = fpCamera->getRealDirection();
		Ogre::Quaternion launchPitchModifier(BONE_PITCH, fpCamera->getRealRight());
		
		launchDirection.normalise();
		launchDirection = launchPitchModifier * launchDirection;

		entityNode->attachObject(boneEntity);
		entityNode->setPosition(bonePosition);
		entityNode->setScale(0.5f, 0.5f, 0.5f);
		entityNode->getUserObjectBindings().setUserAny("solid", Ogre::Any(Ogre::String("true")));
		entityNode->getUserObjectBindings().setUserAny("mass", Ogre::Any(Ogre::Real(0.4f)));
		entityNode->getUserObjectBindings().setUserAny("onCollision", Ogre::Any(Ogre::String("playSfx Crack.ogg;destroy")));
		entityNode->getUserObjectBindings().setUserAny("BoneTag", Ogre::Any(Ogre::String("true")));
		
		mApp->getPropertyParser()->parseHierarchy(entityNode);
		btRigidBody *entityBody = entityNode->getUserObjectBindings().getUserAny("Body").get<btRigidBody*>();
		btVector3 forceVector(launchDirection.x, launchDirection.y, launchDirection.z);
		entityBody->applyCentralImpulse(forceVector * BONE_LAUNCH_FORCE);
		entityBody->applyImpulse(forceVector * BONE_ANGULAR_FORCE, btVector3(0.f, 0.5f, 0.f));
	}
}

void FL::Player::addBones(unsigned int count) {
	boneCounter += count;
	mApp->getLogger()->log(FL::Logger::LOG_INFO, std::to_string(count) + " bones were added. Current count: " + std::to_string(boneCounter));
}

int FL::Player::getBoneCount() {
	return boneCounter;
}

void FL::Player::rotate(Ogre::Radian yaw, Ogre::Radian pitch) {
	Ogre::Matrix3 yawMatrix, pitchMatrix;

	this->yaw += yaw;
	this->pitch += pitch;

	if (this->pitch > MAX_PITCH) { this->pitch = MAX_PITCH; }
	if (this->pitch < MIN_PITCH) { this->pitch = MIN_PITCH; }

	yawMatrix.FromAngleAxis(Ogre::Vector3::UNIT_Y, this->yaw);
	pitchMatrix.FromAngleAxis(Ogre::Vector3::UNIT_X, this->pitch);

	fpCamera->setOrientation(Ogre::Quaternion(yawMatrix * pitchMatrix));

	// Update listener
	Ogre::Vector3 direction = fpCamera->getRealDirection();
	Ogre::Vector3 up = fpCamera->getRealUp();
	sf::Listener::setDirection(direction.x, direction.y, direction.z);
	sf::Listener::setUpVector(up.x, up.y, up.z);
}

void FL::Player::update() {
	if (movingSide || movingForward) { 
		Ogre::Vector3 direction(0, 0, 0);
		if (movingSide) { 
			Ogre::Vector3 rightVector = fpCamera->getRealRight();
			rightVector.normalise();
			rightVector.y = 0;
			if (!right) { rightVector *= -1; }
			direction += rightVector; 
		}
		if (movingForward) { 
			Ogre::Vector3 forwardDirection = fpCamera->getRealDirection();
			forwardDirection.normalise();
			forwardDirection.y = 0;
			if (!forward) { forwardDirection *= -1; }
			direction += forwardDirection; 
		}
		direction.normalise();
		direction *= speed;
		
		const btVector3 &playerVelocity = pBody->getLinearVelocity();
		pBody->setLinearVelocity(btVector3(direction.x, playerVelocity.y(), direction.z));	// Do not alter vertical component

		if (!pBody->isActive()) { pBody->activate(); }

		Ogre::Vector3 pos = fpCamera->getRealPosition();
		Ogre::Vector3 bodyPos = static_cast<Ogre::Node*>(pBody->getUserPointer())->getPosition();
		sf::Listener::setPosition(pos.x, pos.y, pos.z);
		mApp->getAudioManager()->playSfx("Footsteps.ogg", bodyPos, true);
	}
	else {
		const btVector3 &playerVelocity = pBody->getLinearVelocity();
		pBody->setLinearVelocity(btVector3(0, playerVelocity.y(), 0));
		mApp->getAudioManager()->pauseSfx("Footsteps.ogg");
	}
}

void FL::Player::getRayCastEnds(btVector3 & start, btVector3 & end) {
	Ogre::Vector3 cameraWorldPosition, rayCastForwardVector, rayCastEnd;

	cameraWorldPosition = fpCamera->getDerivedPosition();
	rayCastForwardVector = fpCamera->getRealDirection() * RAYCAST_LENGTH;
	rayCastEnd = cameraWorldPosition + rayCastForwardVector;

	start.setX(cameraWorldPosition.x);
	start.setY(cameraWorldPosition.y);
	start.setZ(cameraWorldPosition.z);

	end.setX(rayCastEnd.x);
	end.setY(rayCastEnd.y);
	end.setZ(rayCastEnd.z);
}

void FL::Player::setSpeed(Ogre::Real speed) {
	this->speed = speed;
}

bool FL::Player::getFlag(Ogre::String flagName) {
	if (mFlags.count(flagName)) { return mFlags[flagName]; }
	else { 
		std::string msg = "Invalid player flag name: " + flagName;
		throw new std::exception(msg.c_str()); 
	}
}

void FL::Player::setFlag(Ogre::String flagName, bool value) {
	if (mFlags.count(flagName)) { mFlags[flagName] = value; }
	else { 
		std::string msg = "Invalid player flag name: " + flagName;
		throw new std::exception(msg.c_str()); 
	}
}
