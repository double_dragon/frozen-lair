#include "CallbackParser.h"
#include "FrozenLairApplication.h"
#include "Physics.h"
#include "Player.h"

FL::Physics::Physics(FL::FrozenLairApplication *mApp, Ogre::SceneManager *mSceneMgr) {
	collisionConfiguration = new btDefaultCollisionConfiguration();
	collisionDispatcher = new btCollisionDispatcher(collisionConfiguration);
	overlappingPairCache = new btDbvtBroadphase();
	solver = new btSequentialImpulseConstraintSolver();
	world = new btDiscreteDynamicsWorld(collisionDispatcher, overlappingPairCache, solver, collisionConfiguration);
	world->setGravity(btVector3(0, -GRAVITY, 0));

	if (mSceneMgr) { mDebugDrawer = new CDebugDraw(mSceneMgr, world); }

	drawDebug = false;
	mDebugDrawer->setDebugMode(btIDebugDraw::DBG_NoDebug);

	this->mApp = mApp;
}

FL::Physics::~Physics() {
	for (int i = world->getNumCollisionObjects() - 1; i >= 0; i--) {
		btCollisionObject* obj = world->getCollisionObjectArray()[i];
		btRigidBody* body = btRigidBody::upcast(obj);

		if (body && body->getMotionState()) {
			delete body->getMotionState();
		}

		world->removeCollisionObject(obj);
		if (obj) { delete obj; }
	}

	for (unsigned int i = 0; i < collisionShapes.size(); i++) {
		btCollisionShape* shape = collisionShapes[i];
		collisionShapes[i] = 0;
		delete shape;
	}

	for (unsigned int i = 0; i < triangleMeshes.size(); i++) {
		btTriangleMesh *mesh = triangleMeshes[i];
		triangleMeshes[i] = 0;
		delete mesh;
	}

	if (mDebugDrawer) { delete mDebugDrawer; }
	if (world) { delete world; }
	if (solver) { delete solver; }
	if (overlappingPairCache) { delete overlappingPairCache; }
	if (collisionDispatcher) { delete collisionDispatcher; }
	if (collisionConfiguration) { delete collisionConfiguration; }
}

void FL::Physics::update(Ogre::Real delta) {
	world->stepSimulation(delta, MAX_SUBSTEPS);
	handleCollisions();

	if (mDebugDrawer) { mDebugDrawer->Update(); }

	for (int i = 0; i < world->getNumCollisionObjects(); i++) {
		btCollisionObject *obj = world->getCollisionObjectArray()[i];
		btRigidBody *body = btRigidBody::upcast(obj);
		Ogre::Node *node = static_cast<Ogre::Node*>(obj->getUserPointer());

		if (body) {
			if (body->getMotionState()) {
				btTransform trans;
				body->getMotionState()->getWorldTransform(trans);

				if (node) {
					//	Small caveat here. Bullet works in world space. As such, coordinates must be 
					//	handled by it. This overrides Ogre's handling of the node hierarchy, thus
					//	invalidating any node parenting transformation.

					btVector3 position = trans.getOrigin();
					btQuaternion orientation = trans.getRotation();
					node->_setDerivedPosition(Ogre::Vector3(position.getX(), position.getY(), position.getZ()));
					node->_setDerivedOrientation(Ogre::Quaternion(orientation.getW(), orientation.getX(), orientation.getY(), orientation.getZ()));
				}
			}
		}
	}
}

void FL::Physics::addBody(btRigidBody * body, btCollisionShape * shape, btTriangleMesh *mesh) {
	collisionShapes.push_back(shape);
	world->addRigidBody(body);
	if (mesh) { triangleMeshes.push_back(mesh); }
}

void FL::Physics::addTrigger(btCollisionObject * trigger, btCollisionShape * shape) {
	trigger->setCollisionFlags(trigger->getCollisionFlags() | btCollisionObject::CF_NO_CONTACT_RESPONSE);
	collisionShapes.push_back(shape);
	world->addCollisionObject(trigger);
}

void FL::Physics::remove(btCollisionObject * object, btCollisionShape * shape) {
	btRigidBody *body = btRigidBody::upcast(object);

	if (body && body->getMotionState()) {
		delete body->getMotionState();
	}

	world->removeCollisionObject(object);
	if (object) { delete object; }

	auto idx = std::find(collisionShapes.begin(), collisionShapes.end(), shape);
	if (idx != collisionShapes.end()) { collisionShapes.erase(idx); }
	delete shape;
}

Ogre::Node * FL::Physics::doRayCastFromPlayer() {
	btVector3 start, end;
	mApp->getPlayer()->getRayCastEnds(start, end);
	return doRayCast(start, end);
}

Ogre::Node* FL::Physics::doRayCast(const btVector3& start, const btVector3& end) {
	btCollisionWorld::ClosestRayResultCallback rayCastCallback(start, end);
	world->rayTest(start, end, rayCastCallback);

	if (rayCastCallback.hasHit()) {
		const btCollisionObject *hit = rayCastCallback.m_collisionObject;
		if (hit) {
			Ogre::Node *node = static_cast<Ogre::Node*>(hit->getUserPointer());
			return node;
		}
		else {
			return nullptr;
		}
	}
	else {
		return nullptr;
	}
}

bool FL::Physics::toggleDebugDraw() {
	drawDebug = !drawDebug;
	
	if (!drawDebug) { mDebugDrawer->setDebugMode(btIDebugDraw::DBG_NoDebug); } 
	else { mDebugDrawer->setDebugMode(btIDebugDraw::DBG_DrawWireframe); }
	
	return drawDebug;
}

void FL::Physics::handleCollisions() {
	int numManifolds = world->getDispatcher()->getNumManifolds();
	for (int i = 0; i<numManifolds; i++)
	{
		btPersistentManifold* contactManifold = world->getDispatcher()->getManifoldByIndexInternal(i);
		const btCollisionObject* obA = contactManifold->getBody0();
		const btCollisionObject* obB = contactManifold->getBody1();

		int numContacts = contactManifold->getNumContacts();
		for (int j = 0; j<numContacts; j++)
		{
			btManifoldPoint& pt = contactManifold->getContactPoint(j);
			if (pt.getDistance()<0.f)
			{
				const btVector3& ptA = pt.getPositionWorldOnA();
				const btVector3& ptB = pt.getPositionWorldOnB();
				const btVector3& normalOnB = pt.m_normalWorldOnB;

				onCollisionCallback(pt, obA, obB);
				break;
			}
		}
	}
}

bool FL::Physics::onCollisionCallback(btManifoldPoint & cp, const btCollisionObject * bc0, const btCollisionObject * bc1) {
	Ogre::SceneNode *n0 = static_cast<Ogre::SceneNode*>(bc0->getUserPointer());
	Ogre::SceneNode *n1 = static_cast<Ogre::SceneNode*>(bc1->getUserPointer());
	Ogre::UserObjectBindings &ob0 = n0->getUserObjectBindings();
	Ogre::UserObjectBindings &ob1 = n1->getUserObjectBindings();
	bool collided = false;

	// At the moment, only one-shot triggers are supported
	if (!ob0.getUserAny("onCollision").isEmpty()) {
		ob0.eraseUserAny("onCollisionAfter");
		mApp->getCallbackParser()->parse(n0, ob0.getUserAny("onCollision").get<Ogre::String>(), n1);
		ob0.eraseUserAny("onCollision");
		if (!ob0.getUserAny("onCollisionAfter").isEmpty()) {
			mApp->getCallbackParser()->parse(n0, ob0.getUserAny("onCollisionAfter").get<Ogre::String>(), n1);
		}
		collided = true;
	}
	if (!ob1.getUserAny("onCollision").isEmpty()) {
		ob1.eraseUserAny("onCollisionAfter");
		mApp->getCallbackParser()->parse(n1, ob1.getUserAny("onCollision").get<Ogre::String>(), n0);
		ob1.eraseUserAny("onCollision");
		if (!ob1.getUserAny("onCollisionAfter").isEmpty()) {
			mApp->getCallbackParser()->parse(n1, ob1.getUserAny("onCollisionAfter").get<Ogre::String>(), n0);
		}
		collided = true;
	}
	
	return collided;
}
