#pragma once

#include <stack>

#include <OISMouse.h>

#include <CEGUI\InputEvent.h>

#include "InputCommons.h"

namespace FL {
	class FrozenLairApplication;

	namespace Input {
		enum InputState;

		class MouseHandler : public OIS::MouseListener
		{
		public:
			MouseHandler(FL::FrozenLairApplication* app);
			~MouseHandler();

			void setState(FL::Input::InputState state);
			void revertState();

			virtual bool mouseMoved(const OIS::MouseEvent &arg);
			virtual bool mousePressed(const OIS::MouseEvent &arg, OIS::MouseButtonID id);
			virtual bool mouseReleased(const OIS::MouseEvent &arg, OIS::MouseButtonID id);
		private:
			const double MOUSE_SCALE = 1500;
			const float LOCKPICK_MOUSE_SENSIBILITY = 5;
			
			FL::FrozenLairApplication *mApp = nullptr;
			std::stack<FL::Input::InputState> stateStack;
			FL::Input::InputState inputState;

			bool mouseMovedDefault(const OIS::MouseEvent &arg);
			bool mouseMovedCEGUI(const OIS::MouseEvent &arg);
			bool mouseMovedDialog(const OIS::MouseEvent &arg);
			bool mouseMovedLockpick(const OIS::MouseEvent &arg);

			bool mousePressedDefault(const OIS::MouseEvent &arg, OIS::MouseButtonID id);
			bool mousePressedCEGUI(const OIS::MouseEvent &arg, OIS::MouseButtonID id);
			bool mousePressedDialog(const OIS::MouseEvent &arg, OIS::MouseButtonID id);
			bool mousePressedLockpick(const OIS::MouseEvent &arg, OIS::MouseButtonID id);

			bool mouseReleasedDefault(const OIS::MouseEvent &arg, OIS::MouseButtonID id);
			bool mouseReleasedCEGUI(const OIS::MouseEvent &arg, OIS::MouseButtonID id);			
			bool mouseReleasedDialog(const OIS::MouseEvent &arg, OIS::MouseButtonID id);
			bool mouseReleasedLockpick(const OIS::MouseEvent &arg, OIS::MouseButtonID id);

			CEGUI::MouseButton convertButton(OIS::MouseButtonID buttonID);
		};
	}
}