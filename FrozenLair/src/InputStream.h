#pragma once

#include <Ogre.h>
#include <SFML\System\InputStream.hpp>

namespace FL {
	class FrozenLairApplication;
	class InputStream : public sf::InputStream {
	public:
		InputStream(FL::FrozenLairApplication *mApp, Ogre::String mResourceGroupName = Ogre::ResourceGroupManager::DEFAULT_RESOURCE_GROUP_NAME);
		~InputStream();

		bool open(std::string filename);
		sf::Int64 read(void* data, sf::Int64 size);
		sf::Int64 seek(sf::Int64 position);
		sf::Int64 tell();
		sf::Int64 getSize();

	private:
		FL::FrozenLairApplication *mApp;

		Ogre::String mResourceGroupName;
		Ogre::DataStreamPtr mOgreStream;
	};
}