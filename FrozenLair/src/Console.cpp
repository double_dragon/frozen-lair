#include "CallbackParser.h"
#include "Console.h"
#include "FrozenLairApplication.h"

FL::Console::Console(CEGUI::Window *mWindow, FL::FrozenLairApplication *mApp) : msgQueue() {
	this->mWindow = mWindow;
	this->mApp = mApp;
	msgQueue.push_back(mWindow->getChild("ConsoleOutput")->getText());
	mWindow->getChild("ConsoleInput")->subscribeEvent(CEGUI::Editbox::EventTextAccepted, CEGUI::Event::Subscriber(&Console::onTextSubmitted, this));
}

FL::Console::~Console() {
}

void FL::Console::toggle() {
	if (mWindow->isVisible()) { mWindow->getChild("ConsoleInput")->deactivate(); }
	mWindow->setVisible(!mWindow->isVisible());
	if (mWindow->isVisible()) { mWindow->getChild("ConsoleInput")->activate(); }
}

bool FL::Console::isActive() {
	return mWindow->isVisible();
}

void FL::Console::log(Logger::LogLevel level, std::string msg) {
	append(CEGUI::String(msg));
}

bool FL::Console::onTextSubmitted(const CEGUI::EventArgs & args) {
	CEGUI::Editbox* mBox = static_cast<CEGUI::Editbox*>(mWindow->getChild("ConsoleInput"));
	mApp->getCallbackParser()->parse(nullptr, mBox->getText().c_str());
	mBox->setText("");	
	return true;
}

void FL::Console::append(CEGUI::String msg) {
	CEGUI::String newText = "";
	CEGUI::Window *consoleOutput = mWindow->getChild("ConsoleOutput");

	msgQueue.push_back(msg);
	if (msgQueue.size() > MAX_LINES) { msgQueue.pop_front(); }
	
	for (unsigned int i = 0; i < msgQueue.size(); i++) {
		newText.append(msgQueue[i]);
		newText.append("\n");
	}

	consoleOutput->setText(newText);
}
