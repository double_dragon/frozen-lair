#include <Ogre.h>

#include "AudioManager.h"
#include "InputStream.h"

FL::AudioManager::AudioManager(FL::FrozenLairApplication *mApp) {
	this->mApp = mApp;

	mBgmInputStream = new FL::InputStream(mApp, "Sounds");

	if (!mBgmInputStream->open("Crystal-Caverns.ogg") || !mBgm.openFromStream(*mBgmInputStream)) {
		OGRE_EXCEPT(Ogre::Exception::ERR_FILE_NOT_FOUND, "Couldn't load BGM file", "FL::AudioManager");
	}

	mBgm.setLoop(true);
	mBgm.setVolume(10);
}

// Note: deletion of input streams is taken care of by Music and Sound
FL::AudioManager::~AudioManager() {
	mBgm.stop();
	
	for (auto sfxIterator = sfxs.begin(); sfxIterator != sfxs.end(); sfxIterator++) {
		sfxIterator->second.stop();
		if (sfxIterator->second.getBuffer()) { delete sfxIterator->second.getBuffer(); }
	}
}

void FL::AudioManager::startBGM() {
	mBgm.play();
}

void FL::AudioManager::stopBGM() {
	mBgm.stop();
}

void FL::AudioManager::playSfx(Ogre::String sfxName, Ogre::Vector3 origin, bool loop) {
	if (!sfxs.count(sfxName) && !loadSfx(sfxName)) { return; }

	if (origin != Ogre::Vector3::ZERO) {
		sfxs[sfxName].setPosition(origin.x, origin.y, origin.z);
	}

	if (loop && sfxs[sfxName].getStatus() == sf::SoundSource::Playing) {
		return;
	}
	else {
		sfxs[sfxName].setLoop(loop);
		sfxs[sfxName].play();
	}
}

void FL::AudioManager::stopSfx(Ogre::String sfxName) {
	if (sfxs.count(sfxName)) {
		sfxs[sfxName].stop();
	}
}

void FL::AudioManager::pauseSfx(Ogre::String sfxName) {
	if (sfxs.count(sfxName)) {
		sfxs[sfxName].pause();
	}
}

bool FL::AudioManager::loadSfx(Ogre::String sfxName) {
	FL::InputStream *sfxStream = new FL::InputStream(mApp, "Sounds");
	sf::SoundBuffer *sfxBuffer = new sf::SoundBuffer();

	if (!sfxStream->open(sfxName) || !sfxBuffer->loadFromStream(*sfxStream)) { return false; }
	sfxs[sfxName].setBuffer(*sfxBuffer);
	sfxs[sfxName].setAttenuation(0.2f);
	return true;
}
