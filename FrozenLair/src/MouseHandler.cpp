#include "CallbackParser.h"
#include "DialogManager.h"
#include "FrozenLairApplication.h"
#include "LockpickGame.h"
#include "Physics.h"
#include "Player.h"
#include "MouseHandler.h"

FL::Input::MouseHandler::MouseHandler(FL::FrozenLairApplication * app) : inputState(IS_IGNORE) {
	mApp = app;
}

FL::Input::MouseHandler::~MouseHandler() {
}

void FL::Input::MouseHandler::setState(FL::Input::InputState state) {
	stateStack.push(inputState);
	inputState = state;
}

void FL::Input::MouseHandler::revertState() {
	if (stateStack.size() > 0) {
		inputState = stateStack.top();
		stateStack.pop();
	}
}

bool FL::Input::MouseHandler::mouseMoved(const OIS::MouseEvent & arg) {
	switch (inputState) {
	case IS_IGNORE:
	case IS_OUTRO:
		return false;
	case IS_CEGUI:
		return mouseMovedCEGUI(arg);
	case IS_DIALOG:
		return mouseMovedDialog(arg);
	case IS_LOCKPICK:
		return mouseMovedLockpick(arg);
	case IS_CONSOLE:
	case IS_DEFAULT:
	default:
		return mouseMovedDefault(arg);
	}

	return false;
}

bool FL::Input::MouseHandler::mousePressed(const OIS::MouseEvent & arg, OIS::MouseButtonID id) {
	switch (inputState) {
	case IS_IGNORE:
	case IS_OUTRO:
		return false;
	case IS_CEGUI:
		return mousePressedCEGUI(arg, id);
	case IS_DIALOG:
		return mousePressedDialog(arg, id);
	case IS_LOCKPICK:
		return mousePressedLockpick(arg, id);
	case IS_CONSOLE:
	case IS_DEFAULT:
	default:
		return mousePressedDefault(arg, id);
	}

	return false;
}

bool FL::Input::MouseHandler::mouseReleased(const OIS::MouseEvent & arg, OIS::MouseButtonID id) {
	switch (inputState) {
	case IS_IGNORE:
	case IS_OUTRO:
		return false;
	case IS_CEGUI:
		return mouseReleasedCEGUI(arg, id);
	case IS_DIALOG:
		return mouseReleasedDialog(arg, id);
	case IS_LOCKPICK:
		return mouseReleasedLockpick(arg, id);
	case IS_CONSOLE:
	case IS_DEFAULT:
	default:
		return mouseReleasedDefault(arg, id);
	}

	return false;
}

bool FL::Input::MouseHandler::mouseMovedDefault(const OIS::MouseEvent & arg) {
	Ogre::Real x((float)arg.state.X.rel), y((float)arg.state.Y.rel);
	FL::Player* player = mApp->getPlayer();
	Ogre::Radian yaw(Ogre::Real(-x / MOUSE_SCALE)), pitch(Ogre::Real(-y / MOUSE_SCALE));

	player->rotate(yaw, pitch);
	return true;
}

bool FL::Input::MouseHandler::mouseMovedCEGUI(const OIS::MouseEvent & arg) {
	CEGUI::System::getSingleton().getDefaultGUIContext().injectMouseMove((float)arg.state.X.rel, (float)arg.state.Y.rel);
	return true;
}

bool FL::Input::MouseHandler::mouseMovedDialog(const OIS::MouseEvent & arg) {
	return false;
}

bool FL::Input::MouseHandler::mouseMovedLockpick(const OIS::MouseEvent & arg) {
	if (arg.state.X.rel > LOCKPICK_MOUSE_SENSIBILITY) {
		mApp->getLockpickGame()->shiftRight();
		return true;
	}
	else if (arg.state.X.rel < -LOCKPICK_MOUSE_SENSIBILITY) {
		mApp->getLockpickGame()->shiftLeft();
		return true;
	}
	else if (arg.state.Y.rel < - LOCKPICK_MOUSE_SENSIBILITY) {
		mApp->getLockpickGame()->tilt();
		return true;
	}

	return false;
}

bool FL::Input::MouseHandler::mousePressedDefault(const OIS::MouseEvent & arg, OIS::MouseButtonID id) {
	if (id == OIS::MouseButtonID::MB_Left) {
		btVector3 start, end;
		mApp->getPlayer()->getRayCastEnds(start, end);
		Ogre::Node *node = mApp->getPhysics()->doRayCast(start, end);

		if (node) {
			Ogre::Any onInteraction = node->getUserObjectBindings().getUserAny("onInteraction");
			if (!onInteraction.isEmpty()) {
				Ogre::String onInteractCallbackString = onInteraction.get<Ogre::String>();
				mApp->getCallbackParser()->parse(node, onInteractCallbackString);
			}
		}

		return true;
	}
	else if (id == OIS::MouseButtonID::MB_Right) {
		mApp->getPlayer()->throwBone();
	}

	return false;
}

bool FL::Input::MouseHandler::mousePressedCEGUI(const OIS::MouseEvent & arg, OIS::MouseButtonID id) {
	CEGUI::System::getSingleton().getDefaultGUIContext().injectMouseButtonDown(convertButton(id));
	return true;
}

bool FL::Input::MouseHandler::mousePressedDialog(const OIS::MouseEvent & arg, OIS::MouseButtonID id) {
	if (id == OIS::MouseButtonID::MB_Right) {
		mApp->getDialogManager()->step();
		return true;
	}
	return false;
}

bool FL::Input::MouseHandler::mousePressedLockpick(const OIS::MouseEvent & arg, OIS::MouseButtonID id) {
	FL::LockpickGame *lockpickGame = mApp->getLockpickGame();

	if (id == OIS::MB_Left) {
		lockpickGame->lock();
		if (lockpickGame->allLocked()) {
			lockpickGame->unlock();
			mApp->getPlayer()->setFlag("fHasTreasure", true);
			mApp->getSceneManager()->getSceneNode("rope")->getUserObjectBindings().setUserAny("onInteraction", Ogre::Any(Ogre::String("dialog Leave")));
			mApp->getSceneManager()->getSceneNode("Dragon")->getUserObjectBindings().setUserAny("onInteraction", Ogre::Any(Ogre::String("dialog ExamineDragonTreasureTaken")));
			mApp->getCallbackParser()->parse(nullptr, "dialog TreasureGot");
		}
	}
	else if (id == OIS::MB_Right) {
		lockpickGame->stop();
	}

	return false;
}

bool FL::Input::MouseHandler::mouseReleasedDefault(const OIS::MouseEvent & arg, OIS::MouseButtonID id) {
	return false;
}

bool FL::Input::MouseHandler::mouseReleasedCEGUI(const OIS::MouseEvent & arg, OIS::MouseButtonID id) {
	CEGUI::System::getSingleton().getDefaultGUIContext().injectMouseButtonUp(convertButton(id));
	return true;
}

bool FL::Input::MouseHandler::mouseReleasedDialog(const OIS::MouseEvent & arg, OIS::MouseButtonID id) {
	return false;
}

bool FL::Input::MouseHandler::mouseReleasedLockpick(const OIS::MouseEvent & arg, OIS::MouseButtonID id) {
	return false;
}

CEGUI::MouseButton FL::Input::MouseHandler::convertButton(OIS::MouseButtonID buttonID) {
	switch (buttonID)
	{
	case OIS::MB_Left:
		return CEGUI::MouseButton::LeftButton;

	case OIS::MB_Right:
		return CEGUI::MouseButton::RightButton;

	case OIS::MB_Middle:
		return CEGUI::MouseButton::MiddleButton;

	default:
		return CEGUI::MouseButton::LeftButton;
	}
}