#pragma once

#include <deque>

#include <CEGUI\CEGUI.h>

#include "Logger.h"

namespace FL {
	class FrozenLairApplication;

	/** Interface to the development console window. */
	class Console : public FL::Logger {
	public:
		Console(CEGUI::Window *mWindow, FL::FrozenLairApplication *mApp);
		~Console();

		/** Hides/shows the development console */
		void toggle();

		bool isActive();

		/** Logs the message on the console window. @see Logger */
		virtual void log(Logger::LogLevel level, std::string msg);
	
	private:
		const unsigned int MAX_LINES = 20;

		std::deque<CEGUI::String> msgQueue;
		CEGUI::Window *mWindow;
		FL::FrozenLairApplication *mApp;
		
		/** Subscriber function for text input. */
		bool onTextSubmitted(const CEGUI::EventArgs &args);

		/** Attaches a new string to the console log. */
		void append(CEGUI::String msg);
	};
}