#include <BulletCollision\CollisionDispatch\btGhostObject.h>

#include "AudioManager.h"
#include "CallbackParser.h"
#include "CustomPropertyParser.h"
#include "Console.h"
#include "DialogManager.h"
#include "FrozenLairApplication.h"
#include "GUIManager.h"
#include "KeyboardHandler.h"
#include "LockpickGame.h"
#include "MouseHandler.h"
#include "Physics.h"
#include "Player.h"
#include "ShaderManager.h"

FL::CallbackParser::CallbackParser(FL::FrozenLairApplication *mApp) : timer(0.0f) {
	this->mApp = mApp;

	callbacks["echo"] = &FL::CallbackParser::parseEcho;
	callbacks["dialog"] = &FL::CallbackParser::parseDialog;
	callbacks["hint"] = &FL::CallbackParser::parseHint;
	callbacks["addBones"] = &FL::CallbackParser::parseAddBones;
	callbacks["barrierScript"] = &FL::CallbackParser::parseBarrierScript;
	callbacks["destroy"] = &FL::CallbackParser::parseDestroy;
	callbacks["startLockpick"] = &FL::CallbackParser::parseStartLockpick;
	callbacks["bbox"] = &FL::CallbackParser::parseBbox;
	callbacks["exit"] = &FL::CallbackParser::parseExit;
	callbacks["setUserAny"] = &FL::CallbackParser::parseSetUserAny;
	callbacks["playSfx"] = &FL::CallbackParser::parsePlaySfx;
	callbacks["outro"] = &FL::CallbackParser::parseOutro;
	callbacks["corpseScript"] = &FL::CallbackParser::parseCorpseScript;
	callbacks["treasureScript"] = &FL::CallbackParser::parseTreasureScript;
	callbacks["lockpickScript"] = &FL::CallbackParser::parseLockpickScript;
	callbacks["causeRipples"] = &FL::CallbackParser::parseCauseRipples;
	callbacks["spawnDroplet"] = &FL::CallbackParser::parseSpawnDroplet;
	callbacks["setPlayerSpeed"] = &FL::CallbackParser::parseSetPlayerSpeed;
}

FL::CallbackParser::~CallbackParser() {
	for (unsigned int i = 0; i < scheduled.size() ; i++) {
		ScheduledCallback *sc = scheduled[i];
		scheduled[i] = nullptr;
		delete sc;
	}
}

void FL::CallbackParser::schedule(Ogre::Node * sender, Ogre::String callback, Ogre::Real duration, bool repeat) {
	ScheduledCallback *sc = new ScheduledCallback();
	sc->sender = sender;
	sc->callback = callback;
	sc->duration = duration;
	sc->repeat = repeat;
	sc->start = timer;
	scheduled.push_back(sc);
}

void FL::CallbackParser::parse(Ogre::Node *sender, Ogre::String commands, Ogre::Node *cause) {
	Ogre::StringVector parseMultiple = Ogre::StringUtil::split(commands, ";");

	for (unsigned int i = 0; i < parseMultiple.size(); i++) {
		Ogre::String command = parseMultiple[i];
		Ogre::StringVector parseResult = Ogre::StringUtil::split(command, " ", 1);
		Ogre::String cmd, args;

		if (parseResult.size() == 1) { args = ""; }
		else if (parseResult.size() == 2) { args = parseResult[1]; }
		else { return; }

		cmd = parseResult[0];
		if (callbacks.count(cmd)) {
			auto callback = callbacks[cmd];
			(this->*callback)(sender, args, cause);
		}
	}
}

void FL::CallbackParser::update(Ogre::Real delta) {
	timer += delta;

	for (auto it = scheduled.begin(); it != scheduled.end(); it++) {
		ScheduledCallback *sc = *it;
		Ogre::Real elapsed = timer - sc->start;
		if (elapsed > sc->duration) {
			parse(sc->sender, sc->callback);
			if (sc->repeat) {
				sc->start = timer;
			}
			else {
				scheduled.erase(it);
				delete sc;
				it--;
			}
		}
	}
}

void FL::CallbackParser::parseEcho(Ogre::Node * sender, Ogre::String message, Ogre::Node *cause){
	FL::Logger *mLogger = mApp->getLogger();
	if (mLogger) { mLogger->log(FL::Logger::LOG_INFO, message); }
}

void FL::CallbackParser::parseDialog(Ogre::Node * sender, Ogre::String dialogId, Ogre::Node *cause) {
	FL::DialogManager *dialogManager = mApp->getDialogManager();

	dialogManager->step(dialogId);
	mApp->getLogger()->log(FL::Logger::LOG_INFO, "Requested dialog id: " + dialogId);
	mApp->getPlayer()->stop(true, true);
	mApp->getKeyboardHandler()->setState(FL::Input::IS_DIALOG);
	mApp->getMouseHandler()->setState(FL::Input::IS_DIALOG);
}

void FL::CallbackParser::parseBbox(Ogre::Node * sender, Ogre::String msg, Ogre::Node *cause) {
	Ogre::String state = mApp->getPhysics()->toggleDebugDraw() ? "True" : "False";
	mApp->getLogger()->log(FL::Logger::LOG_INFO, "Bound box draw set to: " + state);
}

void FL::CallbackParser::parseHint(Ogre::Node * sender, Ogre::String argString, Ogre::Node *cause) {
	Ogre::StringVector args = Ogre::StringUtil::split(argString);
	Ogre::String text = "", icon = "";

	if (args.size() >= 1) {
		text = args[0];
	}
	if (args.size() >= 2) {
		icon = args[1];
	}

	mApp->getGUIManager()->showHint(text, icon);
}

void FL::CallbackParser::parseExit(Ogre::Node * sender, Ogre::String msg, Ogre::Node *cause) {
	mApp->stop();
}

void FL::CallbackParser::parseStartLockpick(Ogre::Node * sender, Ogre::String msg, Ogre::Node *cause) {
	mApp->getLockpickGame()->start();
}

void FL::CallbackParser::parseAddBones(Ogre::Node * sender, Ogre::String count, Ogre::Node *cause) {
	try {
		int countNumber = std::stoi(count);
		if (countNumber > 0) {
			mApp->getPlayer()->addBones(countNumber);
		}
	}
	catch (std::invalid_argument ia) {
		mApp->getLogger()->log(FL::Logger::LOG_WARNING, "Bone count is invalid");
	}
}

void FL::CallbackParser::parseBarrierScript(Ogre::Node * sender, Ogre::String arg, Ogre::Node *cause) {
	static unsigned int state = 0;

	if (!sender) { mApp->getLogger()->log(FL::Logger::LOG_ERROR, "Barrier script was called but node data was not given"); return; }
	if (!cause) { mApp->getLogger()->log(FL::Logger::LOG_ERROR, "Barrier script was called but cause data was not given"); return; }

	if (cause->getUserObjectBindings().getUserAny("BoneTag").isEmpty()) { 
		sender->getUserObjectBindings().setUserAny("onCollisionAfter", Ogre::Any(Ogre::String("setUserAny onCollision barrierScript"))); // Must reset trigger
		return; 
	} // Something else collided, ignore

	Ogre::SceneNode *asSceneNode = static_cast<Ogre::SceneNode*>(sender);
	if(!asSceneNode) { mApp->getLogger()->log(FL::Logger::LOG_ERROR, "Barrier script was called but node data was not a SceneNode"); return; }
	Ogre::Entity *entity = static_cast<Ogre::Entity*>(asSceneNode->getAttachedObject(0));

	if (state == 0) {
		mApp->getSceneManager()->destroyMovableObject(entity);
		Ogre::Entity * b1 = mApp->getSceneManager()->createEntity("icewall_bit.mesh");
		asSceneNode->attachObject(b1);
		asSceneNode->getUserObjectBindings().setUserAny("onCollisionAfter", Ogre::Any(Ogre::String("setUserAny onCollision barrierScript")));
		asSceneNode->getUserObjectBindings().setUserAny("onInteraction", Ogre::Any(Ogre::String("dialog ExamineBarrier1")));
		state++;
	}
	else if (state == 1) {
		mApp->getSceneManager()->destroyMovableObject(entity);
		Ogre::Entity * b2 = mApp->getSceneManager()->createEntity("icewall_lot.mesh");
		asSceneNode->attachObject(b2);
		asSceneNode->getUserObjectBindings().setUserAny("onCollisionAfter", Ogre::Any(Ogre::String("setUserAny onCollision barrierScript")));
		asSceneNode->getUserObjectBindings().setUserAny("onInteraction", Ogre::Any(Ogre::String("dialog ExamineBarrier2")));
		state++;
	}
	else if (state == 2) {
		parseDestroy(sender, arg, cause);
		mApp->getPlayer()->setFlag("fDestroyedBarrier", true);
	}
}

void FL::CallbackParser::parseDestroy(Ogre::Node * sender, Ogre::String msg, Ogre::Node *cause) {
	if (!sender) { mApp->getLogger()->log(FL::Logger::LOG_ERROR, "Node data required for deletion"); return; }
	
	Ogre::SceneNode *asSceneNode = static_cast<Ogre::SceneNode*>(sender);
	if (!asSceneNode) { mApp->getLogger()->log(FL::Logger::LOG_ERROR, "Node data is not a Scene Node"); return; }
	mApp->destroyObject(asSceneNode);
}

void FL::CallbackParser::parseSetUserAny(Ogre::Node * sender, Ogre::String arg, Ogre::Node * cause) {
	Ogre::StringVector args = Ogre::StringUtil::split(arg, " ", 1);

	if (args.size() >= 2) {
		sender->getUserObjectBindings().setUserAny(args[0], Ogre::Any(args[1]));
	}
}

void FL::CallbackParser::parsePlaySfx(Ogre::Node * sender, Ogre::String sfx, Ogre::Node * cuase) {
	Ogre::Vector3 origin = sender ? sender->_getDerivedPosition() : Ogre::Vector3::ZERO;
	mApp->getAudioManager()->playSfx(sfx, origin);
}

void FL::CallbackParser::parseOutro(Ogre::Node * sender, Ogre::String sfx, Ogre::Node * cause) {
	mApp->startOutro();
}

void FL::CallbackParser::parseCorpseScript(Ogre::Node * sender, Ogre::String arg, Ogre::Node * cause) {
	static unsigned int bonesTaken = 0;
	Player *mPlayer = mApp->getPlayer();
	if (!mPlayer->getFlag("fReadJournal")) {
		parseDialog(sender, "CorpseFirstTime", cause);
		mPlayer->setFlag("fReadJournal", true);
	}
	else if (mPlayer->getFlag("fDestroyedBarrier")) {
		parseDialog(sender, "CorpseNoNeed", cause);
	}
	else {
		if (mPlayer->getBoneCount() > 0) {
			parseDialog(sender, "CorpseAlreadyHave", cause);
		}
		else if (bonesTaken == 0) {
			parseDialog(sender, "CorpseGetBoneOnce", cause);
			bonesTaken++;
		}
		else if (bonesTaken == 1) {
			parseDialog(sender, "CorpseGetBoneTwice", cause);
			bonesTaken++;
		}
		else {
			parseDialog(sender, "CorpseGetBoneMore", cause);
		}
	}
}

void FL::CallbackParser::parseTreasureScript(Ogre::Node * sender, Ogre::String arg, Ogre::Node * cause) {
	Player *mPlayer = mApp->getPlayer();

	if (!mPlayer->getFlag("fHasCheckedTreasure")) {
		parseDialog(sender, "TreasureFirstTime", cause);
		mPlayer->setFlag("fHasCheckedTreasure", true);
	}
	else if (!mPlayer->getFlag("fHasLockpick")) {
		parseDialog(sender, "TreasureLocked", cause);
	}
	else if (!mPlayer->getFlag("fHasTreasure")) {
		parseDialog(sender, "TreasureWithLockpick", cause);
	}
	else {
		parseDialog(sender, "TreasureAlreadyTaken", cause);
	}
}

void FL::CallbackParser::parseLockpickScript(Ogre::Node * sender, Ogre::String arg, Ogre::Node * cause) {
	Player *mPlayer = mApp->getPlayer();

	if (!mPlayer->getFlag("fHasCheckedTreasure")) {
		parseDialog(sender, "LockpickFirstTime", cause);
	}
	else if (!mPlayer->getFlag("fHasLockpick")) {
		Ogre::Node *lockpick = mApp->getSceneManager()->getSceneNode("lockpick");
		if (!lockpick) {
			OGRE_EXCEPT(Ogre::Exception::ERR_ITEM_NOT_FOUND, "Could not locate lockpick node", "CallbackParser::parseLockpickScript");
		}
		parseDestroy(lockpick, "", cause);
		mPlayer->setFlag("fHasLockpick", true);
		parseDialog(sender, "LockpickAfterTreasure", cause);
	}
}

void FL::CallbackParser::parseCauseRipples(Ogre::Node * sender, Ogre::String arg, Ogre::Node * cause) {
	if (!sender) {
		mApp->getLogger()->log(FL::Logger::LOG_ERROR, "Can't start ripple script, sender missing");
		return;
	}

	sender->getUserObjectBindings().setUserAny("onCollisionAfter", Ogre::Any(Ogre::String("setUserAny onCollision causeRipples")));

	if (!cause) {
		mApp->getLogger()->log(FL::Logger::LOG_ERROR, "Can't start ripple script, cause missing");
		return;
	}

	if (!cause->getUserObjectBindings().getUserAny("DropletTag").isEmpty()) {
		mApp->getAudioManager()->playSfx("CaveWaterDrop.ogg", sender->getPosition());
		mApp->getShaderManager()->startRipples(static_cast<Ogre::SceneNode*>(sender));
	}
}

void FL::CallbackParser::parseSpawnDroplet(Ogre::Node * sender, Ogre::String arg, Ogre::Node * cause) {
	if (!sender) {
		mApp->getLogger()->log(FL::Logger::LOG_ERROR, "Can't spawn droplet, no calling node was set");
		return;
	}

	Ogre::SceneManager *mSceneMgr = mApp->getSceneManager();
	Ogre::SceneNode *entityNode = mSceneMgr->getRootSceneNode()->createChildSceneNode();
	Ogre::Entity *dropletEntity = mSceneMgr->createEntity("Droplet.mesh");
	Ogre::Vector3 dropletPosition = sender->_getDerivedPosition();

	entityNode->attachObject(dropletEntity);
	entityNode->setPosition(dropletPosition);
	entityNode->setScale(0.45f, 0.45f, 0.45f);
	entityNode->getUserObjectBindings().setUserAny("solid", Ogre::Any(Ogre::String("sphere")));
	entityNode->getUserObjectBindings().setUserAny("mass", Ogre::Any(Ogre::Real(0.005f)));
	entityNode->getUserObjectBindings().setUserAny("damp", Ogre::Any(Ogre::Real(0.9f)));
	entityNode->getUserObjectBindings().setUserAny("onCollision", Ogre::Any(Ogre::String("destroy")));
	entityNode->getUserObjectBindings().setUserAny("DropletTag", Ogre::Any(Ogre::String("true")));

	mApp->getPropertyParser()->parseHierarchy(entityNode);
}

void FL::CallbackParser::parseSetPlayerSpeed(Ogre::Node * sender, Ogre::String arg, Ogre::Node * cause) {
	Ogre::Real s = Ogre::StringConverter::parseReal(arg);
	mApp->getPlayer()->setSpeed(s);
}
