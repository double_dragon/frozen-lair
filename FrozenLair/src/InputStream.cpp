#include "FrozenLairApplication.h"
#include "Logger.h"
#include "InputStream.h"

FL::InputStream::InputStream(FL::FrozenLairApplication *mApp, Ogre::String mResourceGroupName) {
	this->mResourceGroupName = mResourceGroupName;
	this->mApp = mApp;
}

FL::InputStream::~InputStream() {
	mOgreStream->close();
}

bool FL::InputStream::open(std::string filename) {
	try {
		if (mOgreStream.get()) { mOgreStream->close(); }
		mOgreStream = Ogre::ResourceGroupManager::getSingleton().openResource(filename, mResourceGroupName);
	}
	catch (Ogre::Exception e) {
		mApp->getLogger()->log(FL::Logger::LOG_ERROR, e.getDescription());
		return false;
	}
	return true;
}

sf::Int64 FL::InputStream::read(void * data, sf::Int64 size) {
	return mOgreStream->read(data, (size_t)size);
}

sf::Int64 FL::InputStream::seek(sf::Int64 position) {
	mOgreStream->seek((size_t)position);
	return position;
}

sf::Int64 FL::InputStream::tell() {
	return mOgreStream->tell();
}

sf::Int64 FL::InputStream::getSize() {
	return mOgreStream->size();
}
