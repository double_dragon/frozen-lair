#pragma once

#include <Ogre.h>

namespace FL {
	class FrozenLairApplication;

	class ShaderManager {
	public:
		ShaderManager(FL::FrozenLairApplication *app);

		void startRipples(Ogre::SceneNode *nodeName);

	private:
		FL::FrozenLairApplication *mApp;
	};
}