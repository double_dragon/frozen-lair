#pragma once

#include <CEGUI\CEGUI.h>

namespace FL {
	class FrozenLairApplication;

	class LockpickGame {

	public:
		LockpickGame(FL::FrozenLairApplication *app, CEGUI::Window *root);
		~LockpickGame();

		void shiftLeft();
		void shiftRight();
		void tilt();
		bool lock();
		bool allLocked();
		void start();
		void stop();
		void unlock();
		void reset();
		void update(Ogre::Real delta);

	private:

		class Tumble {
		public:
			Tumble(CEGUI::Window *tumbleWindow);
			
			void tilt();
			bool isShifting();
			bool isLocked();
			bool lock();
			void reset();
			void update(Ogre::Real delta);

		private:
			const float TUMBLE_SPEED_BASE;
			const float TUMBLE_SPEED_DELTA;
			const float TUMBLE_OFFSET;
			const float TUMBLE_THRESHOLD;

			bool shifting = false;
			bool locked = false;
			float currentSpeed;
			CEGUI::Window *mWindow;
			CEGUI::UVector2 basePos;
		};

		class Pick {
		public:
			Pick(CEGUI::Window *pickWindow);

			void tilt();
			void shiftLeft();
			void shiftRight();
			bool isShifting();
			bool isSliding();
			unsigned int getIdx();
			void update(Ogre::Real delta);

		private:
			const float PICK_SPEED;
			const float PICK_OFFSET;
			const float PICK_STEP;

			CEGUI::Window *mWindow;
			CEGUI::UVector2 basePos;
			bool shiftingTop = false, shiftingLeft = false, shiftingRight = false;
			unsigned int idx = 0;

		};

		const static unsigned int NTUMBLES = 7;
		
		Tumble *mTumbles[NTUMBLES];
		Pick *mPick;
		
		CEGUI::Window *mRoot;
		FL::FrozenLairApplication *mApp;
	};

}