#include "CallbackParser.h"
#include "FrozenLairApplication.h"
#include "GUIManager.h"
#include "Physics.h"
#include "Player.h"

FL::GUIManager::GUIManager(FL::FrozenLairApplication * mApp, CEGUI::Window * mGUIRoot) : cState(GS_INTRO), FADE_SPEED(0.2f) {
	this->mApp = mApp;
	this->mGUIRoot = mGUIRoot;
}

void FL::GUIManager::update(Ogre::Real delta) {
	switch (cState) {
	case GS_INTRO:
		updateIntro(delta);
		break;

	case GS_GAME:
		updateGame(delta);
		break;

	case GS_OUTRO:
		updateOutro(delta);
		break;
	default:
		break;
	}
}

void FL::GUIManager::showHint(Ogre::String text, Ogre::String icon) {
	CEGUI::Window *mHint = mGUIRoot->getChild("Overlay/Hint");

	if (text.size() > 0 || icon.size() > 0) {
		if (!mHint->isVisible()) { mHint->setVisible(true); }
		if (text.size() > 0) {
			mHint->getChild("HintText")->setProperty("Text", text);
		}
		if (icon.size() > 0) {
			mHint->getChild("HintIcon")->setProperty("Image", icon);
		}
	}
}

void FL::GUIManager::hideHint() {
	CEGUI::Window *mHint = mGUIRoot->getChild("Overlay/Hint");
	if (mHint->isVisible()) { mHint->setVisible(false); }
}

void FL::GUIManager::startOutro() {
	cState = GS_OUTRO;
	mGUIRoot->getChild("Overlay")->setVisible(false);
	CEGUI::Window *mSplashScreens = mGUIRoot->getChild("SplashScreens");
	mSplashScreens->setVisible(true);
	mSplashScreens->setAlpha(0.f);
	mSplashScreens->getChild("Credits")->setVisible(true);
}

void FL::GUIManager::setShowBoneHint(bool show) {
	CEGUI::Window *boneHint = mGUIRoot->getChild("Overlay/BoneHint");
	if (boneHint->isVisible() != show) { boneHint->setVisible(show); }
}

void FL::GUIManager::updateIntro(Ogre::Real delta) {
	CEGUI::Window *mSplashScreens = mGUIRoot->getChild("SplashScreens");
	float alpha = mSplashScreens->getAlpha();
	alpha -= FADE_SPEED * delta;
	if (alpha > 0) {
		mSplashScreens->setAlpha(alpha);
	}
	else {
		mSplashScreens->setVisible(false);
		mGUIRoot->getChild("Overlay")->setVisible(true);
		cState = GS_GAME;
		mApp->startGame();
	}
}

void FL::GUIManager::updateGame(Ogre::Real delta) {
	Ogre::Node *hoverNode = mApp->getPhysics()->doRayCastFromPlayer();

	if (hoverNode) {
		Ogre::UserObjectBindings &userObjects = hoverNode->getUserObjectBindings();
		if (!userObjects.getUserAny("onHover").isEmpty()) {
			Ogre::String onHover = userObjects.getUserAny("onHover").get<Ogre::String>();
			mApp->getCallbackParser()->parse(hoverNode, onHover);
		}
		else { hideHint(); }
	}
	else { hideHint(); }

	setShowBoneHint(mApp->getPlayer()->getBoneCount() > 0);
}

void FL::GUIManager::updateOutro(Ogre::Real delta) {
	CEGUI::Window *mSplashScreens = mGUIRoot->getChild("SplashScreens");
	float alpha = mSplashScreens->getAlpha();
	alpha += FADE_SPEED * delta;
	alpha = alpha < 1.f ? alpha : 1.f;
	mSplashScreens->setAlpha(alpha);
}
