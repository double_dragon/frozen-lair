#pragma once

// Ogre header
#include <Ogre.h>

// Required for .scene loading
#include <Plugins\DSInterface\dotSceneProcessor.h>

// OIS (input system) headers
#include <OIS.h>

// CEGUI (User Interface) modules
#include <CEGUI\CEGUI.h>
#include <CEGUI\RendererModules\Ogre\Renderer.h>

namespace FL {
	class AudioManager;
	class Console;
	class CustomPropertyParser;
	class CallbackParser;
	class Logger;
	class Player;
	class Physics;
	class DialogManager;
	class GUIManager;
	class LockpickGame;
	class ShaderManager;

	namespace Input {
		class KeyboardHandler;
		class MouseHandler;
	}

	class FrozenLairApplication : public Ogre::WindowEventListener, Ogre::FrameListener
	{
	public:
		FrozenLairApplication();
		virtual ~FrozenLairApplication();

		bool go();
		void stop();
		void startGame();
		void startOutro();
		void destroyObject(Ogre::SceneNode *node);

		FL::AudioManager *getAudioManager();
		FL::Player *getPlayer();
		FL::Logger *getLogger();
		FL::Console *getConsole();
		FL::Physics *getPhysics();
		FL::DialogManager *getDialogManager();
		FL::LockpickGame *getLockpickGame();
		FL::CallbackParser *getCallbackParser();
		FL::CustomPropertyParser *getPropertyParser();
		FL::Input::KeyboardHandler *getKeyboardHandler();
		FL::Input::MouseHandler *getMouseHandler();
		FL::GUIManager *getGUIManager();
		FL::ShaderManager *getShaderManager();

		Ogre::SceneManager *getSceneManager();

	protected:
		// Ogre::WindowEventListener
		virtual void windowResized(Ogre::RenderWindow* rw);
		virtual void windowClosed(Ogre::RenderWindow* rw);
		virtual void windowFocusChange(Ogre::RenderWindow* rw);

		// Ogre::FrameListener
		virtual bool frameStarted(const Ogre::FrameEvent& fe);
		virtual bool frameRenderingQueued(const Ogre::FrameEvent& fe);
		virtual bool frameEnded(const Ogre::FrameEvent& fe);

	private:
		// Game-specific elements
		FL::AudioManager *mAudioManager = nullptr;
		FL::Physics *mPhysics = nullptr;
		FL::Console *mConsole = nullptr;
		FL::Player *mPlayer = nullptr;
		FL::CustomPropertyParser *mPropertyParser = nullptr;
		FL::CallbackParser *mCallbackParser = nullptr;
		FL::GUIManager *mGUIManager = nullptr;
		FL::DialogManager *mDialogManager = nullptr;
		FL::LockpickGame *mLockpickGame = nullptr;
		FL::ShaderManager *mShaderManager = nullptr;

		// Ogre structures
		Ogre::Root *mRoot = nullptr;
		Ogre::RenderWindow *mWindow = nullptr;
		Ogre::SceneManager *mSceneMgr = nullptr;
		Ogre::Camera *mCamera = nullptr;
		Ogre::String mResourceCfg;
		Ogre::String mPluginsCfg;

		// OIS structures
		OIS::InputManager *mInputManager = nullptr;
		OIS::Mouse *mMouse = nullptr;
		OIS::Keyboard *mKeyboard = nullptr;

		// CEGUI structures
		CEGUI::OgreRenderer *mGuiRenderer = nullptr;
		CEGUI::Window *mRootWindow = nullptr;

		FL::Input::KeyboardHandler *mKeyboardListener = nullptr;
		FL::Input::MouseHandler *mMouseListener = nullptr;
		bool running = true;
		std::vector<Ogre::SceneNode*> deletionStack;

		// Book-keeping methods for startup
		bool startup();
		void loadScene();
		void startOIS();
		void startCEGUI();
		void initialiseGame();

		void deleteObjects();
	};
}