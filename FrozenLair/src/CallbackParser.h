#pragma once

#include <Ogre.h>

namespace FL {
	class FrozenLairApplication;

	/**
	 * This is the core external scripting handler. Callbacks are simplified procedure calls in the form
	 * of <callback_name> <args> that can be specified on external files to have a certain effect on the 
	 * running game. Examples include showing dialogs, UI hints, but also object-specific scripts.
	 */
	class CallbackParser {
	public:
		CallbackParser(FL::FrozenLairApplication *mApp);
		~CallbackParser();

		/**
		 * Schedules a callback to be executed at a later time, possibly repeating itself.
		 * @param sender the node executing this callback.
		 * @param callback the full calback.
		 * @param repeat whether the callback should be rescheduled after executing.
		 * @see ScheduledCallback
		 */
		void schedule(Ogre::Node *sender, Ogre::String callback, Ogre::Real duration, bool repeat = false);

		/**
		 * Executes a callback.
		 * @param sender the node executing the callback (can be null depending on the callback).
		 * @param comamnds the callback itself.
		 * @param cause the node who triggered the callback execution (e.g. an item that collided with the sender).
		 */
		void parse(Ogre::Node *sender, Ogre::String commands, Ogre::Node *cause = nullptr);

		/** 
		 * Updates the callback scheduler.
		 * @param delta time in seconds since last call.
		 */
		void update(Ogre::Real delta);

	private:
		/** Holds all references for a callback to be scheduled. */
		typedef struct ScheduledCallback {
			Ogre::String callback;
			Ogre::Node *sender;
			Ogre::Real start;
			Ogre::Real duration;
			bool repeat;
		} ScheduledCallback;

		std::map<Ogre::String, void(FL::CallbackParser::*)(Ogre::Node*, Ogre::String, Ogre::Node*)> callbacks;
		std::vector<ScheduledCallback*> scheduled;
		FL::FrozenLairApplication *mApp;
		Ogre::Real timer;

		/** [Syntax: echo <msg>] Logs the message passed as argument. */
		void parseEcho(Ogre::Node *sender, Ogre::String message, Ogre::Node *cause);
		/** [Syntax: dialog <dialogID>] Starts the dialog window for a given dialog ID. @see DialogManager */
		void parseDialog(Ogre::Node *sender, Ogre::String dialogId, Ogre::Node *cause);
		/** [Syntax: bbox] Toggles debug draw for physics objects. */
		void parseBbox(Ogre::Node *sender, Ogre::String msg, Ogre::Node *cause);
		/** 
		 * [Syntax: hint <text> {<picture>}]
		 * Shows a hint on top right of the crosshair. The hint comprises a text message and optionally
		 * a small icon, which must be loaded and referenced to within a CEGUI Imageset.
		 */
		void parseHint(Ogre::Node *sender, Ogre::String argString, Ogre::Node *cause);
		/** [Syntax: exit] Closes the game. */
		void parseExit(Ogre::Node *sender, Ogre::String msg, Ogre::Node *cause);
		/** [Syntax: startLockpick] Starts the lockpicking minigame. @see LockpickGame */
		void parseStartLockpick(Ogre::Node *sender, Ogre::String msg, Ogre::Node *cause);
		/** [Syntax: addBones <n>] Adds n bones to the player's inventory. */
		void parseAddBones(Ogre::Node *sender, Ogre::String count, Ogre::Node *cause);
		/** [Syntax: barrierScript] Executes the script for the barrier minigame */
		void parseBarrierScript(Ogre::Node *sender, Ogre::String arg, Ogre::Node *cause);
		/** [Syntax: destroy] Deletes the calling object from the scene, as well as any physics property. */
		void parseDestroy(Ogre::Node *sender, Ogre::String msg, Ogre::Node *cause);
		/** [Syntax: setUserAny <custom_property>] Sets a custom property for the calling object. @see CustomPropertyParser */
		void parseSetUserAny(Ogre::Node *sender, Ogre::String arg, Ogre::Node *cause);
		/** [Syntax: playSfx <sfx_name>] Plays a sound effect, originating from the sender's position. */
		void parsePlaySfx(Ogre::Node *sender, Ogre::String sfx, Ogre::Node *cause);
		/** [Syntax: outro] Executes the game outro sequence, showing credits. */
		void parseOutro(Ogre::Node *sender, Ogre::String sfx, Ogre::Node *cause);
		/** [Syntax: corpseScript] Script specific for the adventurer's corpse. */
		void parseCorpseScript(Ogre::Node *sender, Ogre::String arg, Ogre::Node *cause);
		/** [Syntax: treasureScript] Script specific for the treasure chest. */
		void parseTreasureScript(Ogre::Node *sender, Ogre::String arg, Ogre::Node *cause);
		/** [Syntax: lockpickScript] Script specific for the adventurer's corpse. */
		void parseLockpickScript(Ogre::Node *sender, Ogre::String arg, Ogre::Node *cause);
		/** [Syntax: causeRipples] Script triggering the ripple effect on specific water surfaces. */
		void parseCauseRipples(Ogre::Node *sender, Ogre::String arg, Ogre::Node *cause);
		/** [Syntax: spawnDroplet] Creates a small drop of water at the sender's position. */
		void parseSpawnDroplet(Ogre::Node *sender, Ogre::String arg, Ogre::Node *cause);
		/** [Syntax: setPlayerSpeed <speed>] Sets the player's speed. */
		void parseSetPlayerSpeed(Ogre::Node *sender, Ogre::String arg, Ogre::Node *cause);
	};
}