#pragma once

#include <string>

namespace FL {

	class Logger {
	public:
		enum LogLevel { LOG_INFO, LOG_WARNING, LOG_ERROR };
		virtual void log(LogLevel level, std::string msg) = 0;
	};

}