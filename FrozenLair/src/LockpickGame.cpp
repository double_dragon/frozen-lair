#include "AudioManager.h"
#include "CallbackParser.h"
#include "FrozenLairApplication.h"
#include "KeyboardHandler.h"
#include "LockpickGame.h"
#include "MouseHandler.h"
#include "Player.h"

FL::LockpickGame::Tumble::Tumble(CEGUI::Window * tumbleWindow) : TUMBLE_SPEED_BASE(0.12f), TUMBLE_SPEED_DELTA(0.04f), TUMBLE_OFFSET(0.1f), TUMBLE_THRESHOLD(0.04f) {
	this->mWindow = tumbleWindow;
	basePos = CEGUI::UVector2(tumbleWindow->getPosition());
}

void FL::LockpickGame::Tumble::tilt() {
	if (!locked) {
		mWindow->setPosition(basePos - CEGUI::UVector2(CEGUI::UDim(0.f, 0.f), CEGUI::UDim(TUMBLE_OFFSET, 0.f)));
		shifting = true;
		currentSpeed = TUMBLE_SPEED_BASE + Ogre::Math::RangeRandom(-1.f, 1.f) * TUMBLE_SPEED_DELTA;
	}
}

bool FL::LockpickGame::Tumble::isShifting() {
	return shifting;
}

bool FL::LockpickGame::Tumble::isLocked() {
	return locked;
}

bool FL::LockpickGame::Tumble::lock() {
	if (!shifting) { return true; }		// Not shifting, so not an error
	else {
		shifting = false;
		float deltaSpace = (basePos - mWindow->getPosition()).d_y.d_scale;
		if (deltaSpace > TUMBLE_THRESHOLD) {	// Tumble successfully locked
			locked = true;
			mWindow->setPosition(basePos - CEGUI::UVector2(CEGUI::UDim::zero(), CEGUI::UDim(TUMBLE_OFFSET, 0.f)));
			return true;
		}
		else {		// Error, signal Game to reset all.
			return false;
		}
	}
}

void FL::LockpickGame::Tumble::reset() {
	shifting = false;
	locked = false;
	mWindow->setPosition(basePos);
}

void FL::LockpickGame::Tumble::update(Ogre::Real delta) {
	if (shifting) {
		mWindow->setPosition(mWindow->getPosition() + CEGUI::UVector2(CEGUI::UDim::zero(), CEGUI::UDim(currentSpeed * delta, 0)));
		float deltaSpace = (basePos - mWindow->getPosition()).d_y.d_scale;
		if (deltaSpace <= 0) { reset(); }
	}
}

FL::LockpickGame::Pick::Pick(CEGUI::Window * pickWindow) : PICK_SPEED(0.12f), PICK_OFFSET(0.04f), PICK_STEP(0.04f) {
	this->mWindow = pickWindow;
	basePos = CEGUI::UVector2(pickWindow->getPosition());
}

void FL::LockpickGame::Pick::tilt() {
	if (!isShifting()) {
		shiftingTop = true;
		mWindow->setPosition(basePos - CEGUI::UVector2(CEGUI::UDim::zero(), CEGUI::UDim(PICK_OFFSET, 0)));
	}
}

void FL::LockpickGame::Pick::shiftLeft() {
	if (!isShifting() && idx > 0) {
		idx--;
		shiftingLeft = true;
		basePos = mWindow->getPosition() - CEGUI::UVector2(CEGUI::UDim(PICK_STEP, 0), CEGUI::UDim::zero());
	}
}

void FL::LockpickGame::Pick::shiftRight() {
	if (!isShifting() && idx < NTUMBLES - 1) {
		idx++;
		shiftingRight = true;
		basePos = mWindow->getPosition() + CEGUI::UVector2(CEGUI::UDim(PICK_STEP, 0), CEGUI::UDim::zero());
	}
}

bool FL::LockpickGame::Pick::isShifting() {
	return shiftingTop || shiftingLeft || shiftingRight;
}

bool FL::LockpickGame::Pick::isSliding() {
	return shiftingLeft || shiftingRight;
}

unsigned int FL::LockpickGame::Pick::getIdx() {
	return idx;
}

void FL::LockpickGame::Pick::update(Ogre::Real delta) {
	if (shiftingTop) {
		mWindow->setPosition(mWindow->getPosition() + CEGUI::UVector2(CEGUI::UDim::zero(), CEGUI::UDim(PICK_SPEED * delta, 0)));
		float deltaSpace = (basePos - mWindow->getPosition()).d_y.d_scale;
		if (deltaSpace < 0) {
			shiftingTop = false;
			mWindow->setPosition(basePos);
		}
	}
	else if (shiftingLeft) {
		mWindow->setPosition(mWindow->getPosition() - CEGUI::UVector2(CEGUI::UDim(PICK_SPEED * delta, 0), CEGUI::UDim::zero()));
		float deltaSpace = (mWindow->getPosition() - basePos).d_x.d_scale;
		if (deltaSpace < 0) {
			shiftingLeft = false;
			mWindow->setPosition(basePos);
		}
	}
	else if (shiftingRight) {
		mWindow->setPosition(mWindow->getPosition() + CEGUI::UVector2(CEGUI::UDim(PICK_SPEED * delta, 0), CEGUI::UDim::zero()));
		float deltaSpace = (basePos - mWindow->getPosition()).d_x.d_scale;
		if (deltaSpace < 0) {
			shiftingRight = false;
			mWindow->setPosition(basePos);
		}
	}
}

FL::LockpickGame::LockpickGame(FL::FrozenLairApplication * app, CEGUI::Window * root) {
	this->mApp = app;
	this->mRoot = root;

	mPick = new Pick(root->getChild("Pick"));
	for (unsigned int i = 0; i < NTUMBLES; i++) {
		mTumbles[i] = new Tumble(root->getChild("Tumble_" + std::to_string(i)));
	}
}

FL::LockpickGame::~LockpickGame() {
	delete mPick;
	for (unsigned int i = 0; i < NTUMBLES; i++) {
		delete mTumbles[i];
	}
}

void FL::LockpickGame::shiftLeft() {
	mPick->shiftLeft();
}

void FL::LockpickGame::shiftRight() {
	mPick->shiftRight();
}

void FL::LockpickGame::tilt() {
	if (!mPick->isShifting()) {
		mPick->tilt();
		if (!mTumbles[mPick->getIdx()]->isLocked()) {
			mApp->getAudioManager()->playSfx("Tick.ogg");
		}
		mTumbles[mPick->getIdx()]->tilt();
	}
}

bool FL::LockpickGame::lock() {
	if (mPick->isSliding()) {
		return false;
	}
	else {
		if (mTumbles[mPick->getIdx()]->lock()) {
			return true;
		}
		else {
			mApp->getAudioManager()->playSfx("Lock.ogg");
			reset();
			return false;
		}
	}
}

bool FL::LockpickGame::allLocked() {
	bool allLocked = true;
	
	for (unsigned int i = 0; i < NTUMBLES; i++) {
		allLocked = allLocked && mTumbles[i]->isLocked();
	}

	return allLocked;
}

void FL::LockpickGame::start() {
	reset();
	mRoot->setVisible(true);
	mApp->getKeyboardHandler()->setState(FL::Input::IS_LOCKPICK);
	mApp->getMouseHandler()->setState(FL::Input::IS_LOCKPICK);
}

void FL::LockpickGame::stop() {
	mRoot->setVisible(false);
	mApp->getKeyboardHandler()->revertState();
	mApp->getMouseHandler()->revertState();
}

void FL::LockpickGame::unlock() {
	mApp->getAudioManager()->playSfx("Unlock.ogg");
	stop();
}

void FL::LockpickGame::reset() {
	for (unsigned int i = 0; i < NTUMBLES; i++) {
		mTumbles[i]->reset();
	}
}

void FL::LockpickGame::update(Ogre::Real delta) {
	for (unsigned int i = 0; i < NTUMBLES; i++) {
		mTumbles[i]->update(delta);
	}
	mPick->update(delta);
}
