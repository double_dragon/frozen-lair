#include "Console.h"
#include "FrozenLairApplication.h"
#include "KeyboardHandler.h"
#include "MouseHandler.h"
#include "Player.h"

FL::Input::KeyboardHandler::KeyboardHandler(FL::FrozenLairApplication* app) : inputState(FL::Input::IS_IGNORE) {
	this->mApp = app;
}

void FL::Input::KeyboardHandler::setState(FL::Input::InputState state) {
	stateStack.push(inputState);
	inputState = state;
}

void FL::Input::KeyboardHandler::revertState() {
	if (stateStack.size() > 0) {
		inputState = stateStack.top();
		stateStack.pop();
	}
}

bool FL::Input::KeyboardHandler::keyPressed(const OIS::KeyEvent & key) {
	switch (inputState) {
	case IS_IGNORE:		// Ignore dialogs for now
	case IS_DIALOG:		// No use for lockpick game
	case IS_LOCKPICK:
		return false;
	case IS_OUTRO:
		return keyPressedOutro(key);
	case IS_CEGUI:
	case IS_CONSOLE:
		return keyPressedCEGUI(key);
	case IS_DEFAULT:
	default:
		return keyPressedDefault(key);
	}
}

bool FL::Input::KeyboardHandler::keyReleased(const OIS::KeyEvent & key) {
	switch (inputState) {
	case IS_IGNORE:
	case IS_DIALOG:
		return false;;
	case IS_OUTRO:
		return keyReleasedOutro(key);
	case IS_CEGUI:
	case IS_CONSOLE:
		return keyReleasedCEGUI(key);
	case IS_DEFAULT:
	default:
		return keyReleasedDefault(key);
	}

	return false;
}

bool FL::Input::KeyboardHandler::keyPressedDefault(const OIS::KeyEvent & key) {
	FL::Console *mConsole = mApp->getConsole();

	if (mConsole && key.key == OIS::KC_F1) {
		setState(IS_CONSOLE);
		mApp->getMouseHandler()->setState(IS_CONSOLE);
		mApp->getPlayer()->stop(true, true);
		mConsole->toggle();
		return true;
	}

	if (key.key == OIS::KC_ESCAPE) {
		mApp->stop();
		return true;
	}
	else if (key.key == OIS::KC_W) {
		FL::Player* player = mApp->getPlayer();
		player->moveForward(true);
		return true;
	}
	else if (key.key == OIS::KC_S) {
		FL::Player* player = mApp->getPlayer();
		player->moveForward(false);
		return true;
	}
	else if (key.key == OIS::KC_A) {
		FL::Player* player = mApp->getPlayer();
		player->moveSidewise(false);
		return true;
	}
	else if (key.key == OIS::KC_D) {
		FL::Player* player = mApp->getPlayer();
		player->moveSidewise(true);
		return true;
	}

	else if (key.key == OIS::KC_UP) {
		FL::Player* player = mApp->getPlayer();
		player->rotate(Ogre::Radian(Ogre::Real(Ogre::Real(0))), Ogre::Radian(ROTATE_SCALE));
		return true;
	}
	else if (key.key == OIS::KC_DOWN) {
		FL::Player* player = mApp->getPlayer();
		player->rotate(Ogre::Radian(Ogre::Real(Ogre::Real(0))), Ogre::Radian(-ROTATE_SCALE));
		return true;
	}
	else if (key.key == OIS::KC_LEFT) {
		FL::Player* player = mApp->getPlayer();
		player->rotate(Ogre::Radian(ROTATE_SCALE), Ogre::Radian(Ogre::Real(0)));
		return true;
	}
	else if (key.key == OIS::KC_RIGHT) {
		FL::Player* player = mApp->getPlayer();
		player->rotate(Ogre::Radian(-ROTATE_SCALE), Ogre::Radian(Ogre::Real(0)));
		return true;
	}

	return false;
}

bool FL::Input::KeyboardHandler::keyPressedCEGUI(const OIS::KeyEvent & key) {
	if (key.key == OIS::KC_F1 && inputState == IS_CONSOLE) {
		revertState();
		mApp->getMouseHandler()->revertState();
		mApp->getConsole()->toggle();
		return true;
	}

	CEGUI::GUIContext& guiContext = CEGUI::System::getSingleton().getDefaultGUIContext();
	guiContext.injectKeyDown((CEGUI::Key::Scan)key.key);
	guiContext.injectChar(key.text);
	return true;
}

bool FL::Input::KeyboardHandler::keyPressedOutro(const OIS::KeyEvent & key) {
	if (key.key == OIS::KC_ESCAPE) {
		mApp->stop();
		return true;
	}
	return false;
}

bool FL::Input::KeyboardHandler::keyReleasedDefault(const OIS::KeyEvent & key) {
	if (key.key == OIS::KC_W || key.key == OIS::KC_S) {
		FL::Player* player = mApp->getPlayer();
		player->stop(false, true);
		return true;
	}
	else if (key.key == OIS::KC_A || key.key == OIS::KC_D) {
		FL::Player* player = mApp->getPlayer();
		player->stop(true, false);
		return true;
	}

	return false;
}

bool FL::Input::KeyboardHandler::keyReleasedCEGUI(const OIS::KeyEvent & key) {
	CEGUI::GUIContext& guiContext = CEGUI::System::getSingleton().getDefaultGUIContext();
	guiContext.injectKeyUp((CEGUI::Key::Scan)key.key);
	return true;
}

bool FL::Input::KeyboardHandler::keyReleasedOutro(const OIS::KeyEvent & key) {
	return false;
}
