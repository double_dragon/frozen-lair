#pragma once

namespace FL {
	namespace Input {
		enum InputState {
			IS_DEFAULT,
			IS_IGNORE,
			IS_CEGUI,
			IS_CONSOLE,
			IS_DIALOG,
			IS_LOCKPICK,
			IS_OUTRO
		};
	}
}