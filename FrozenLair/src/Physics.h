#pragma once

// Disable annoying bullet warnings
#pragma warning(push)
#pragma warning(disable : 4305)
#include <btBulletDynamicsCommon.h>
#include <btBulletCollisionCommon.h>
#pragma warning(pop)

#include <Ogre.h>

#include "DebugDraw.h"

namespace FL {
	class FrozenLairApplication;

	class Physics {
	public:
		Physics(FL::FrozenLairApplication *mApp, Ogre::SceneManager *mSceneMgr);
		~Physics();

		void update(Ogre::Real delta);
		void addBody(btRigidBody* body, btCollisionShape* shape, btTriangleMesh *mesh = nullptr);
		void addTrigger(btCollisionObject *trigger, btCollisionShape *shape);
		void remove(btCollisionObject *object, btCollisionShape *shape);
		Ogre::Node* doRayCastFromPlayer();
		Ogre::Node* doRayCast(const btVector3& start, const btVector3& end);
		bool toggleDebugDraw();

	private:
		const float GRAVITY = 100.0f;
		const int MAX_SUBSTEPS = 10;

		CDebugDraw *mDebugDrawer = nullptr;
		bool drawDebug;

		btDefaultCollisionConfiguration* collisionConfiguration = nullptr;
		btCollisionDispatcher* collisionDispatcher = nullptr;
		btBroadphaseInterface* overlappingPairCache = nullptr;
		btSequentialImpulseConstraintSolver* solver = nullptr;
		btDiscreteDynamicsWorld* world = nullptr;

		std::vector<btCollisionShape*> collisionShapes;
		std::vector<btTriangleMesh*> triangleMeshes;

		FL::FrozenLairApplication *mApp = nullptr;

		void handleCollisions();
		bool onCollisionCallback(btManifoldPoint & cp, const btCollisionObject * bc0, const btCollisionObject * bc1);
	};
}