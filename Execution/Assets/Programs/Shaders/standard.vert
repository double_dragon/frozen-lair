#version 120
#extension GL_ARB_gpu_shader5 : enable

attribute vec4 vertex;
attribute vec3 normal;
attribute vec3 tangent;

attribute vec4 uv0;
attribute vec4 uv1;
attribute vec4 uv2;
attribute vec4 uv3;
attribute vec4 uv4;
attribute vec4 uv5;

uniform mat4 InverseTransposeModelViewMatrix;
uniform mat4 ModelViewProjectionMatrix;
uniform mat4 ModelViewMatrix;
uniform mat4 ShadowTextureMatrix;

uniform int UseDisplacement;
uniform sampler2D DisplacementMap;
uniform float DisplacementScale;

varying vec3 position;
varying vec3 vs_normal;
varying mat3 tbn;

varying vec4 ShadowCoord;
varying vec4 TextureCoord;

void main() {
	vs_normal = normalize(mat3(InverseTransposeModelViewMatrix) * normal);
	
	vec3 T = normalize(mat3(InverseTransposeModelViewMatrix) * tangent);
	vec3 B = normalize(mat3(InverseTransposeModelViewMatrix) * cross(normal, tangent));
	vec3 N = normalize(mat3(InverseTransposeModelViewMatrix) * normal);
	tbn =	mat3(	T.x, T.y, T.z,
					B.x, B.y, B.z,
					N.x, N.y, N.z);

	TextureCoord = uv0;
	ShadowCoord = ShadowTextureMatrix * vertex;

	vec4 newVertex = vertex;
	if (UseDisplacement > 0) {
		vec4 dv;
		float df;

		dv = texture2D(DisplacementMap, TextureCoord.xy);
		df = 0.30 * dv.x + 0.59 * dv.y + 0.11 * dv.z;
		newVertex = vec4(normal * df * DisplacementScale, 0.0) + vertex;
	}

	position = vec3(ModelViewMatrix * newVertex);
	gl_Position = ModelViewProjectionMatrix * newVertex;
}