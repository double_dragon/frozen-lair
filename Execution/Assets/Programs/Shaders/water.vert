#define M_PI 3.1415926535897932384626433832795

uniform float start_time;
uniform float current_time;		// Elapsed time
uniform float tf;				// Time frequency (higher => 'faster' propagation)
uniform float sf;				// Spatial frequency (higher => 'denser' wave)
uniform float cutoff;			// Raised cosine bounds (ripples end after having travelled for 1/cutoff spatial units)
uniform float rolloff;			// Raised cosine rolloff ([0, 1], higher => smoother spatial fadeout)
uniform float fadeoff_speed;	// Lower => ripples last longer
uniform vec2 drift_frequency;	// ST texture drift frequency
uniform float drift_scale;

varying vec3 vs_normal;
varying vec3 position;

varying vec4 TextureCoord;

mat4 rotationMatrix(vec3 axis, float angle);
float left_rcos(float f, float r, float T);

void main() 
{
	vec3 tangent;
	vec3 lightDir;
	vec4 newNormal;
	float angle;
	float phase;
	float radius;
	float time = current_time > start_time ? current_time - start_time : 0.0;

	// Since ripples are assumed to be concentrical to the mesh origin,
	// the tangent vector can be obtained from the vertex position 
	// rotated by 90 degrees along the Z axis.
	if (length(vec3(gl_Vertex)) > 0.0) {
		tangent = vec3(gl_Vertex.z, 0.0, -gl_Vertex.x);
	}
	else {
		tangent = vec3(1.0, 0.0, 0.0);	// Anything goes at the origin
	}
	
	radius = sqrt(gl_Vertex.x * gl_Vertex.x + gl_Vertex.z * gl_Vertex.z);
	phase = radius * sf - time * tf;
	
	// Ripple propagation is radial, and the vs_normal vector oscillates between -Pi / 4 and +Pi / 
	angle = M_PI / 4.0 * sin(phase) * left_rcos(radius - time * fadeoff_speed, rolloff, cutoff);
	
	// The pound is assumed to lay on the horizontal plane, thus the starting vs_normal vector is the Y versor.
	// To calculate new vs_normal vector just rotate along the tangent.
	newNormal = rotationMatrix(tangent, angle) * vec4(0.0, 1.0, 0.0, 1.0);
	vs_normal = normalize(gl_NormalMatrix * vec3(newNormal));
	position = vec3(gl_ModelViewMatrix * gl_Vertex);
	
	TextureCoord = gl_TextureMatrix[0] * gl_MultiTexCoord0;
	TextureCoord.st += drift_scale * vec2(sin(drift_frequency.x * current_time), sin(drift_frequency.y * current_time + M_PI / 4.0));
	gl_Position = ftransform();
}

mat4 rotationMatrix(vec3 axis, float angle)
{
    axis = normalize(axis);
    float s = sin(angle);
    float c = cos(angle);
    float oc = 1.0 - c;
    
    return mat4(oc * axis.x * axis.x + c,           oc * axis.x * axis.y - axis.z * s,  oc * axis.z * axis.x + axis.y * s,  0.0,
                oc * axis.x * axis.y + axis.z * s,  oc * axis.y * axis.y + c,           oc * axis.y * axis.z - axis.x * s,  0.0,
                oc * axis.z * axis.x - axis.y * s,  oc * axis.y * axis.z + axis.x * s,  oc * axis.z * axis.z + c,           0.0,
                0.0,                                0.0,                                0.0,                                1.0);
}

// Raised cosine function, cut to allow only negative frequencies
float left_rcos(float f, float r, float T){
	if (f > 0.0) { return 0.0; }
	else if (f > (r-1.0)/(2.0*T)) { return 1.0; }
	else if (f > (-r-1.0)/(2.0*T)) {
		return 0.5 * (1.0 + cos(M_PI * T / r * (abs(f) - (1.0 - r)/(2.0 * T))));
	}
	else{ return 0.0; }
}