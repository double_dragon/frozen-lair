#version 120

uniform sampler2D tex;
uniform sampler2D nor_map;
uniform sampler2D shadow_texture;

uniform int use_nor_map;
uniform int use_shadow_texture;
uniform int use_fresnel;

uniform float n_lights;
uniform float f_bias;
uniform float f_scale;
uniform float f_exp;
uniform vec4 f_color;

varying vec3 position;
varying vec3 vs_normal;

varying mat3 tbn;

varying vec4 ShadowCoord;
varying vec4 TextureCoord;

vec3 incidentLight(gl_LightSourceParameters light, vec3 position);

void main() 
{
	vec4 ambient, diffuse_color, specular_color, texColor;
	vec4 baseColor, shadowColor;
	vec3 n, lightDir, reflect, eye;
	float f;
	float NdotL, EdotR, IdotN;
	
	if (use_shadow_texture > 0) {
		shadowColor = texture2DProj(shadow_texture, ShadowCoord);
	}
	else {
		shadowColor = vec4(1.0, 1.0, 1.0, 1.0);
	}
	
	if (use_nor_map > 0) {
		// Sample normal from normal map
		n = texture2D(nor_map, TextureCoord.st).rgb;
		n = normalize(n * 2.0 - 1.0);
		n = normalize(tbn * n);
	}
	else {
		n = normalize(vs_normal);
	}
	
	eye = normalize(-position);		// Since vertex is in eye space, eye vector is its opposite
	
	ambient = gl_LightModel.ambient * gl_FrontMaterial.ambient;
	for (int i = 0 ; i < int(n_lights) ; i++) {
		ambient += gl_LightSource[i].ambient * gl_FrontMaterial.ambient;
		lightDir = incidentLight(gl_LightSource[i], position);	// Incident light direction
		reflect = -2.0 * n * dot(lightDir, n) + lightDir;		// Reflected ray direction
		NdotL = max(dot(-lightDir, n), 0.0);
		EdotR = max(dot(eye, reflect), 0.0);
		
		if (NdotL > 0.0) {
			diffuse_color += gl_FrontMaterial.diffuse * gl_LightSource[i].diffuse * NdotL;
		}
		if (EdotR > 0.0) {
			specular_color += gl_FrontMaterial.specular * gl_LightSource[i].specular * pow(EdotR, gl_FrontMaterial.shininess);
		}
	}
	
	texColor = texture2D(tex, TextureCoord.st);
	baseColor = vec4(	(diffuse_color.rgb + ambient.rgb + specular_color.rgb) * texColor.rgb - (1.0 - shadowColor.rgb) * shadowColor.a, 
						(gl_FrontMaterial.diffuse.a + gl_FrontMaterial.specular.a) * texColor.a);
	
	// Compute Fresnel index
	f = 0.0;
	if (use_fresnel > 0) {
		IdotN = min(dot(normalize(position), n), 0.0);
		if (IdotN < 0.0) {
			f = max(0.0, min(1.0, f_bias + f_scale * pow(1.0 + IdotN, f_exp)));
		}
	}
	
	gl_FragColor = mix(baseColor, f_color, f);
}


// Evaluates the incident light direction, based on vertex position in world eye space coordinates
// and light type. Light is scaled down by ambient attenuation
// Kudos to: https://stackoverflow.com/questions/11212933/glsl-gl-lightsource-point-directional-spot-differentiation
vec3 incidentLight (gl_LightSourceParameters light, vec3 position) {
    if (light.position.w == 0.0) {
        return normalize (-light.position.xyz);
    } else {
        vec3 offset = position - light.position.xyz;
        float distance = length (offset);
        vec3 direction = normalize (offset);
        float intensity;
        if (light.spotCutoff <= 90.) {
            float spotCos = dot (direction, normalize (light.spotDirection));
            intensity = pow (spotCos, light.spotExponent) *
                    step (light.spotCosCutoff, spotCos);
        } else {
            intensity = 1.;
        }
        intensity /= light.constantAttenuation +
                light.linearAttenuation * distance +
                light.quadraticAttenuation * distance * distance;
        return intensity * direction;
    }
}